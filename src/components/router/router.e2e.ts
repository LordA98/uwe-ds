import { newE2EPage } from "@stencil/core/testing";

describe("uwe-root", () => {
  it("renders", async () => {
    const page = await newE2EPage();
    await page.setContent("<uwe-router></uwe-router>");

    const element = await page.find("uwe-router");
    expect(element).toHaveClass("hydrated");
  });
});
