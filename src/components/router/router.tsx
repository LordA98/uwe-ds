import { Component, h } from "@stencil/core";

/** @ignore */
@Component({
  tag: "uwe-router",
  styleUrl: "router.css",
  shadow: true
})
export class Router {
  /** @ignore */
  render() {
    return (
      <main>
        <stencil-router>
          <stencil-route-switch scrollTopOffset={0}>
            {/* Home Page */}
            <stencil-route url="/" component="uwe-page" exact={true} />
            {/* Dynamic Test Pages
                Generates single component based on :component value
                Get parameters can also be appended to assign prop value for the component.
                E.g. /test/button?value=submit&type=button&name=buttonName&disabled=false
                No parameters assigns no prop values, which will either result in defaults or empties.
                Used for Pa11y Accessibility testing
            */}
            <stencil-route url="/test/:component" component="uwe-page" />
          </stencil-route-switch>
        </stencil-router>
      </main>
    );
  }
}
