# uwe-root

<!-- Auto Generated Below -->


## Dependencies

### Depends on

- stencil-router
- stencil-route-switch
- stencil-route

### Graph
```mermaid
graph TD;
  uwe-router --> stencil-router
  uwe-router --> stencil-route-switch
  uwe-router --> stencil-route
  style uwe-router fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
