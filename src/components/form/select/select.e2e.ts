import { newE2EPage } from "@stencil/core/testing";

describe("uwe-select", () => {
  /**
   * Standard E2E Tests
   */
  it("renders", async () => {
    const page = await newE2EPage();
    await page.setContent("<uwe-select></uwe-select>");

    const element = await page.find("uwe-select");
    expect(element).not.toBeNull();
    expect(element).toHaveClass("hydrated");
  });

  it("renders with option", async () => {
    const page = await newE2EPage();
    await page.setContent(`
      <uwe-select>
        <uwe-option value="test2">Opt 2</uwe-option>
      </uwe-select>
    `);

    const select = await page.find("uwe-select");
    expect(select).not.toBeNull();
    expect(select).toHaveClass("hydrated");

    const option = await page.find("uwe-select uwe-option");
    expect(option).not.toBeNull();
    expect(option).toHaveClass("hydrated");
    expect(option).not.toHaveClass("selected");
    expect(option).not.toHaveAttribute("selected");
  });

  it("renders with a selected option", async () => {
    const page = await newE2EPage();
    await page.setContent(`
      <uwe-select>
        <uwe-option class="sel" value="test" selected>Opt 1</uwe-option>
      </uwe-select>
    `);

    const select = await page.find("uwe-select");
    expect(select).not.toBeNull();
    expect(select).toHaveClass("hydrated");

    const option = await page.find("uwe-select uwe-option.sel");
    expect(option).not.toBeNull();
    expect(option).toHaveClass("hydrated");
    expect(option).toHaveAttribute("selected");
  });

  it("renders with multiple options", async () => {
    const page = await newE2EPage();
    await page.setContent(`
      <uwe-select>
        <uwe-option class="sel" value="test" selected>Opt 1</uwe-option>
        <uwe-option class="not" value="test2">Opt 2</uwe-option>
      </uwe-select>
    `);

    const select = await page.find("uwe-select");
    expect(select).not.toBeNull();
    expect(select).toHaveClass("hydrated");

    const option1 = await page.find("uwe-select uwe-option.sel");
    expect(option1).not.toBeNull();
    expect(option1).toHaveClass("hydrated");
    expect(option1).toHaveAttribute("selected");

    const option2 = await page.find("uwe-select uwe-option.not");
    expect(option2).not.toBeNull();
    expect(option2).toHaveClass("hydrated");
    expect(option2).not.toHaveAttribute("selected");
  });

  describe("toggle dropdown", () => {
    it("opens the dropdown", async () => {
      const page = await newE2EPage();
      await page.setContent(`
        <uwe-select>
          <uwe-option class="sel" value="test" selected>Opt 1</uwe-option>
          <uwe-option class="not" value="test2">Opt 2</uwe-option>
        </uwe-select>
      `);

      const select = await page.find("uwe-select");
      const child_div = await page.find("uwe-select .custom-select");
      expect(select).toHaveClass("hydrated");

      // Click select to open dropdown
      expect(child_div).not.toHaveClass("open");
      await select.click();
      await page.waitForChanges();
      expect(child_div).toHaveClass("open");
    });

    it("closes the dropdown", async () => {
      const page = await newE2EPage();
      await page.setContent(`
        <uwe-select>
          <uwe-option class="sel" value="test" selected>Opt 1</uwe-option>
          <uwe-option class="not" value="test2">Opt 2</uwe-option>
        </uwe-select>
      `);

      const select = await page.find("uwe-select");
      const child_div = await page.find("uwe-select .custom-select");
      expect(select).toHaveClass("hydrated");

      // Click select to open dropdown
      expect(child_div).not.toHaveClass("open");
      await select.click();
      await page.waitForChanges();
      expect(child_div).toHaveClass("open");

      // Click select to close dropdown
      await select.click();
      await page.waitForChanges();
      expect(child_div).not.toHaveClass("open");
    });
  });

  describe("window click", () => {
    it("closes dropdown if open", async () => {
      const page = await newE2EPage();
      await page.setContent(`
        <uwe-select>
          <uwe-option class="sel" value="test" selected>Opt 1</uwe-option>
          <uwe-option class="not" value="test2">Opt 2</uwe-option>
        </uwe-select>
      `);

      const select = await page.find("uwe-select");
      const child_div = await page.find("uwe-select .custom-select");
      expect(select).toHaveClass("hydrated");

      // Click select to open dropdown
      expect(child_div).not.toHaveClass("open");
      await select.click();
      await page.waitForChanges();
      expect(child_div).toHaveClass("open");

      const body = await page.find("body");
      await body.click();
      await page.waitForChanges();
      expect(child_div).not.toHaveClass("open");
    });

    it("does not open dropdown if closed", async () => {
      const page = await newE2EPage();
      await page.setContent(`
        <uwe-select>
          <uwe-option class="sel" value="test" selected>Opt 1</uwe-option>
          <uwe-option class="not" value="test2">Opt 2</uwe-option>
        </uwe-select>
      `);

      const select = await page.find("uwe-select");
      const child_div = await page.find("uwe-select .custom-select");
      expect(select).toHaveClass("hydrated");
      expect(child_div).not.toHaveClass("open");

      const body = await page.find("body");
      await body.click();
      await page.waitForChanges();
      expect(child_div).not.toHaveClass("open");
    });
  });

  describe("option selected event", () => {
    it("receives event to select new option", async () => {
      const page = await newE2EPage();
      await page.setContent(`
        <uwe-select>
          <uwe-option class="sel" value="test" selected>Opt 1</uwe-option>
          <uwe-option class="not" value="test2">Opt 2</uwe-option>
        </uwe-select>
      `);

      const select = await page.find("uwe-select");
      const option_not = await page.find("uwe-option.not");

      const optSelected = await page.spyOnEvent("optionSelected");

      await select.click();
      await page.waitForChanges();
      await option_not.click();
      await page.waitForChanges();

      expect(optSelected).toHaveReceivedEvent();
    });
  });

  /**
   * Screenshot Tests
   */
  it("renders the standard select", async () => {
    const page = await newE2EPage();

    // Link to global CSS & render select w/ option
    await page.setContent(
      `<link href='http://localhost:3333/build/uwe-ds.css' rel='stylesheet' />
      <uwe-select selectId="select" name="selectForOptionTest">
        <uwe-option value="uwe">UWE</uwe-option>
      </uwe-select>`
    );

    const select = await page.find("uwe-select");
    const child_div = await page.find("uwe-select .custom-select");
    expect(select).toHaveClass("hydrated");
    expect(child_div).not.toHaveClass("open");

    // Capture & compare screenshot
    const results = await page.compareScreenshot();
    expect(results).toMatchScreenshot();
  });

  it("renders the dropdown select when clicked", async () => {
    const page = await newE2EPage();

    // Link to global CSS & render select w/ option
    await page.setContent(
      `<link href='http://localhost:3333/build/uwe-ds.css' rel='stylesheet' />
      <uwe-select selectId="select" name="selectForOptionTest">
        <uwe-option value="uwe">UWE</uwe-option>
      </uwe-select>`
    );

    const select = await page.find("uwe-select");
    const child_div = await page.find("uwe-select .custom-select");
    expect(select).toHaveClass("hydrated");

    // Click select to open dropdown
    expect(child_div).not.toHaveClass("open");
    await select.click();
    await page.waitForChanges();
    expect(child_div).toHaveClass("open");

    // Capture & compare screenshot
    const results = await page.compareScreenshot();
    expect(results).toMatchScreenshot();
  });
});
