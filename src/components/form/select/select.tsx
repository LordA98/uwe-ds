import {
  Component,
  Host,
  h,
  Prop,
  Element,
  State,
  Method,
  Listen
} from "@stencil/core";

/**
 * Select Component.  Example Usage:
 * ```html
 * <uwe-select name="selectName">
 *   <uwe-option value="one" selected>Value 1</uwe-option>
 *   <uwe-option value="two">Value 2</uwe-option>
 *   <uwe-option value="three">Value 3</uwe-option>
 * </uwe-select>
 * ```
 */
@Component({
  tag: "uwe-select",
  styleUrl: "select.css",
  shadow: false,
  assetsDirs: ["../../../assets"]
})
export class Select {
  /** @ignore Select Element */
  select: HTMLDivElement;
  /** @ignore Trigger Element */
  trigger: HTMLDivElement;

  /** @ignore This component */
  @Element() thisSelect: HTMLElement;

  /**
   * @ignore
   * Current arrow image
   * The UNPKG CDN has to be used to store and render images as local assets
   * do not seem to work when integrated into other frameworks.
   */
  @State() image: string =
    "https://unpkg.com/uwe-ds@latest/dist/collection/assets/arrow-down.svg";
  /** @ignore If input is in error state or not - red bordered */
  @State() isError: boolean = false;
  /** @ignore Options */
  @State() options: NodeListOf<HTMLUweOptionElement>;
  /** @ignore Selected Option */
  @State() selectedOption: string;
  /** @ignore Input value for form */
  @State() value: string;

  /**
   * Name of input for forms.
   * @category Required
   */
  @Prop() name!: string;
  /**
   * ID of input
   * @category Optional
   */
  @Prop() selectId?: string;

  /** @ignore */
  componentWillLoad() {
    // TODO: Gives error on textContent sometimes
    // Get option with 'selected' property & set as selectOption & value
    let slotted = Array.from(this.thisSelect.children);
    const selected = slotted.find(opt => opt.hasAttribute("selected"));
    this.selectedOption = selected.textContent;
    this.value = selected.getAttribute("value");
  }

  /** @ignore */
  componentDidLoad() {
    this.trigger = this.thisSelect.querySelector(".custom-select__trigger");
    this.select = this.thisSelect.querySelector(".custom-select");
  }

  /**
   * @ignore
   * Listen for an option to be selected in the dropdown select
   * When one is selected, remove 'selected' class from currently selected
   * option and add it to the newly selected option, then update the preview.
   * @param event Contains Option information
   */
  @Listen("optionSelected")
  optionSelectedHandler(event: CustomEvent) {
    // TODO: Arrow not changing back
    this.image =
      "https://unpkg.com/uwe-ds@latest/dist/collection/assets/arrow-down.svg";

    this.options = this.thisSelect.querySelectorAll("uwe-option");
    this.deselectOptions();
    this.selectOption(event.detail.value);

    this.selectedOption = event.detail.thisOpt.textContent;
    this.value = event.detail.value;
  }

  /**
   * @ignore
   * Close dropdown when outside of it is clicked
   * @param event Window clicked
   */
  @Listen("click", { target: "window" })
  windowClickHandler(event: any) {
    if (!this.select.contains(event.target)) {
      this.closeDropdown();
    }
  }

  /**
   * Set error state to true to turn borde red
   */
  @Method()
  async error() {
    this.isError = true;
  }

  /**
   * Set error state to false & reset value
   * @param errorState
   * @param valueState
   */
  @Method()
  async reset(errorState: boolean = true) {
    if (errorState) this.isError = false;
  }

  /**
   * @ignore
   * Toggle dropdown open / closed depending on caller
   */
  closeDropdown() {
    this.select.classList.remove("open");
    this.trigger.classList.add("border-gray-400");
    this.trigger.classList.remove("active");
  }

  /**
   * @ignore
   * Deselect all child option nodes
   * Only 1 will ever be selected at any one time but easier to deselect them all.
   */
  deselectOptions() {
    this.options.forEach(option => {
      option.deselect();
    });
  }

  /**
   * @ignore
   * Select new option that triggered optionSelected event
   * @param optionToSelect
   */
  selectOption(optionToSelect: string) {
    this.options.forEach(option => {
      if (option.value === optionToSelect) {
        option.select();
      }
    });
  }

  /**
   * @ignore
   * Toggle dropdown open / closed depending on caller
   */
  toggleDropdown() {
    this.select.classList.toggle("open");
    this.trigger.classList.toggle("border-gray-400");
    this.trigger.classList.toggle("active");
  }

  /**
   * @ignore
   * Open dropdown of options
   */
  toggleSelectHandler() {
    this.toggleDropdown();
    this.image =
      "https://unpkg.com/uwe-ds@latest/dist/collection/assets/arrow-up.svg";
  }

  /** @ignore */
  render() {
    return (
      <Host>
        <input value={this.value} type="text" name={this.name} hidden />
        <div class="custom-select-wrapper">
          <div
            class="custom-select"
            onClick={this.toggleSelectHandler.bind(this)}
          >
            <div
              class={
                "custom-select__trigger bg-gray-100 p-2 border-2 justify-between content-center " +
                (this.isError ? " error" : " border-gray-400")
              }
            >
              <span class="default">{this.selectedOption}</span>
              <uwe-image src={this.image} />
            </div>
            <div class="custom-options">
              <slot />
            </div>
          </div>
        </div>
      </Host>
    );
  }
}
