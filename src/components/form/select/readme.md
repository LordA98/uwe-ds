# uwe-select

<!-- Auto Generated Below -->


## Properties

| Property            | Attribute   | Description              | Type     | Default     |
| ------------------- | ----------- | ------------------------ | -------- | ----------- |
| `name` _(required)_ | `name`      | Name of input for forms. | `string` | `undefined` |
| `selectId`          | `select-id` | ID of input              | `string` | `undefined` |


## Methods

### `error() => Promise<void>`

Set error state to true to turn borde red

#### Returns

Type: `Promise<void>`



### `reset(errorState?: boolean) => Promise<void>`

Set error state to false & reset value

#### Returns

Type: `Promise<void>`




## Dependencies

### Used by

 - [uwe-page](../../page)

### Depends on

- [uwe-image](../../image)

### Graph
```mermaid
graph TD;
  uwe-select --> uwe-image
  uwe-page --> uwe-select
  style uwe-select fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
