import { Select } from "./select";
import { Option } from "../option/option";

describe("uwe-select", () => {
  it("builds", () => {
    expect(new Select()).toBeTruthy();
  });

  describe("error", () => {
    it("puts select into error state", () => {
      const select = new Select();
      expect(select.isError).toBe(false);
      select.error();
      expect(select.isError).toBe(true);
    });

    it("leaves select in error state if already in error state", () => {
      const select = new Select();
      select.isError = true;
      expect(select.isError).toBe(true);
      select.error();
      expect(select.isError).toBe(true);
    });
  });

  describe("reset", () => {
    it("resets error state", () => {
      const select = new Select();
      select.isError = true;
      expect(select.isError).toBe(true);
      select.reset();
      expect(select.isError).toBe(false);
    });

    it("leaves error state false if already false", () => {
      const select = new Select();
      expect(select.isError).toBe(false);
      select.reset();
      expect(select.isError).toBe(false);
    });
  });

  describe("deselectOptions", () => {
    it("deselects options", async () => {
      const select = new Select();
      const option1 = new Option();
      const option2 = new Option();
      option1.selected = true;
      option2.selected = false;
      // @ts-ignore
      select.options = [option1, option2];

      expect(select.options[0].selected).toBe(true);
      expect(select.options[1].selected).toBe(false);

      // @ts-ignore
      select.deselectOptions();

      expect(select.options[0].selected).toBe(false);
      expect(select.options[1].selected).toBe(false);
    });
  });

  describe("selectOption", () => {
    it("selects option", async () => {
      const select = new Select();
      const option1 = new Option();
      const option2 = new Option();
      option1.selected = false;
      option1.value = "uwe";
      option2.selected = false;
      // @ts-ignore
      select.options = [option1, option2];

      expect(select.options[0].selected).toBe(false);
      expect(select.options[1].selected).toBe(false);

      // @ts-ignore
      select.selectOption("uwe");

      expect(select.options[0].selected).toBe(true);
      expect(select.options[1].selected).toBe(false);
    });
  });
});
