import { Option } from "./option";

describe("uwe-option", () => {
  it("builds", () => {
    expect(new Option()).toBeTruthy();
  });

  describe("deselect", () => {
    it("deselects the option", () => {
      const option = new Option();
      option.selected = true;
      expect(option.selected).toBe(true);
      option.deselect();
      expect(option.selected).toBe(false);
    });

    it("leaves the option deselected if already deselected", () => {
      const option = new Option();
      expect(option.selected).toBe(false);
      option.deselect();
      expect(option.selected).toBe(false);
    });
  });

  describe("select", () => {
    it("selects the option", () => {
      const option = new Option();
      expect(option.selected).toBe(false);
      option.select();
      expect(option.selected).toBe(true);
    });

    it("leaves the option selected if already selected", () => {
      const option = new Option();
      option.selected = true;
      expect(option.selected).toBe(true);
      option.select();
      expect(option.selected).toBe(true);
    });
  });
});
