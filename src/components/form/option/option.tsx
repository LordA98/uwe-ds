import {
  Component,
  Host,
  h,
  Prop,
  Event,
  EventEmitter,
  Method,
  Element
} from "@stencil/core";

/**
 * Option Component.  Example Usage:
 * ```html
 * <!-- Selected by default -->
 * <uwe-option value="val" selected>Value 1</uwe-option>
 * <!-- Not Selected by default -->
 * <uwe-option value="two">Value 2</uwe-option>
 * ```
 */
@Component({
  tag: "uwe-option",
  styleUrl: "option.css",
  shadow: true
})
export class Option {
  /** @ignore This Option Element */
  @Element() thisOpt: HTMLElement;

  /**
   * Whether the option is currently selected or not
   * @category Optional
   */
  @Prop() selected?: boolean = false;
  /**
   * Form value of option
   * @category Required
   */
  @Prop() value: string;

  /**
   * @ignore
   * When option is clicked in dropdown,
   * bubble event up to select component
   */
  @Event({
    eventName: "optionSelected",
    composed: true,
    cancelable: true,
    bubbles: true
  })
  optionSelected: EventEmitter;

  /**
   * Deselect option from dropdown to remove blue highlighting
   * ```javascript
   * const uweOption = document.querySelector("uwe-option");
   * uweOption.deselect();
   * ```
   */
  @Method()
  async deselect() {
    this.selected = false;
  }

  /**
   * Select option from dropdown to add blue highlighting
   * ```javascript
   * const uweOption = document.querySelector("uwe-option");
   * uweOption.select();
   * ```
   */
  @Method()
  async select() {
    this.selected = true;
  }

  /** @ignore Emit optionSelected event */
  optionSelectHandler() {
    this.optionSelected.emit(this);
  }

  /** @ignore */
  render() {
    return (
      <Host>
        <span
          class={"custom-option" + (this.selected ? " selected" : "")}
          data-value={this.value}
          onClick={this.optionSelectHandler.bind(this)}
        >
          <slot />
        </span>
      </Host>
    );
  }
}
