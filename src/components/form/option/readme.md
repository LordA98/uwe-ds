# uwe-option

<!-- Auto Generated Below -->


## Properties

| Property   | Attribute  | Description                                     | Type      | Default     |
| ---------- | ---------- | ----------------------------------------------- | --------- | ----------- |
| `selected` | `selected` | Whether the option is currently selected or not | `boolean` | `false`     |
| `value`    | `value`    | Form value of option                            | `string`  | `undefined` |


## Events

| Event            | Description | Type               |
| ---------------- | ----------- | ------------------ |
| `optionSelected` |             | `CustomEvent<any>` |


## Methods

### `deselect() => Promise<void>`

Deselect option from dropdown to remove blue highlighting
```javascript
const uweOption = document.querySelector("uwe-option");
uweOption.deselect();
```

#### Returns

Type: `Promise<void>`



### `select() => Promise<void>`

Select option from dropdown to add blue highlighting
```javascript
const uweOption = document.querySelector("uwe-option");
uweOption.select();
```

#### Returns

Type: `Promise<void>`




## Dependencies

### Used by

 - [uwe-page](../../page)

### Graph
```mermaid
graph TD;
  uwe-page --> uwe-option
  style uwe-option fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
