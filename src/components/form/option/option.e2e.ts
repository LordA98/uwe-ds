import { newE2EPage } from "@stencil/core/testing";

describe("uwe-option", () => {
  /**
   * Standard E2E Tests
   */
  it("renders", async () => {
    const page = await newE2EPage();
    await page.setContent("<uwe-option></uwe-option>");

    const element = await page.find("uwe-option");
    expect(element).not.toBeNull();
    expect(element).toHaveClass("hydrated");
  });

  it("displays correct text content", async () => {
    const page = await newE2EPage();
    await page.setContent("<uwe-option>Undergraduate</uwe-option>");

    const element = await page.find("uwe-option");
    expect(element).toEqualText("Undergraduate");
  });

  /**
   * Screenshot Tests
   */
  it("renders the option correctly", async () => {
    const page = await newE2EPage();

    // Link to global CSS & render select w/ option
    await page.setContent(
      `<link href='http://localhost:3333/build/uwe-ds.css' rel='stylesheet' />
      <uwe-select selectId="select" name="selectForOptionTest">
        <uwe-option value="uwe">UWE</uwe-option>
      </uwe-select>`
    );

    const select = await page.find("uwe-select");
    const child_div = await page.find("uwe-select .custom-select");
    expect(select).toHaveClass("hydrated");

    // Click select to open dropdown
    expect(child_div).not.toHaveClass("open");
    await select.click();
    await page.waitForChanges();
    expect(child_div).toHaveClass("open");

    const option = await page.find("uwe-option");
    expect(option).toHaveClass("hydrated");

    // Capture & compare screenshot
    const results = await page.compareScreenshot();
    expect(results).toMatchScreenshot();
  });

  it("renders the option correctly if selected", async () => {
    const page = await newE2EPage();

    // Link to global CSS & render select w/ option
    await page.setContent(
      `<link href='http://localhost:3333/build/uwe-ds.css' rel='stylesheet' />
      <uwe-select selectId="select" name="selectForOptionTest">
        <uwe-option value="uwe" selected>UWE Option</uwe-option>
      </uwe-select>`
    );

    // select = uwe-select, select_child_select = actual select element in comp
    const select = await page.find("uwe-select");
    const child_div = await page.find("uwe-select .custom-select");
    expect(select).toHaveClass("hydrated");

    // Click select to open dropdown
    expect(child_div).not.toHaveClass("open");
    await select.click();
    await page.waitForChanges();
    expect(child_div).toHaveClass("open");

    // select = uwe-select, select_child_select = actual select element in comp
    const option = await page.find("uwe-option");
    const child_span = await page.find("uwe-option >>> .custom-option");
    expect(option).toHaveClass("hydrated");
    expect(child_span).toHaveClass("selected");

    // Capture & compare screenshot
    const results = await page.compareScreenshot();
    expect(results).toMatchScreenshot();
  });

  it("renders the option correctly if not selected", async () => {
    const page = await newE2EPage();

    // Link to global CSS & render select w/ option
    await page.setContent(
      `<link href='http://localhost:3333/build/uwe-ds.css' rel='stylesheet' />
      <uwe-select selectId="select" name="selectForOptionTest">
        <uwe-option class="not" value="not">UWE Option Not Selected</uwe-option>
        <uwe-option class="sel" value="sel" selected>UWE Option Selected</uwe-option>
      </uwe-select>`
    );

    // select = uwe-select, select_child_select = actual select element in comp
    const select = await page.find("uwe-select");
    const child_div = await page.find("uwe-select .custom-select");
    expect(select).toHaveClass("hydrated");

    // Click select to open dropdown
    expect(child_div).not.toHaveClass("open");
    await select.click();
    await page.waitForChanges();
    expect(child_div).toHaveClass("open");

    // select = uwe-select, select_child_select = actual select element in comp
    const option = await page.find("uwe-option.not");
    const child_span = await page.find("uwe-option.not >>> .custom-option");
    expect(option).toHaveClass("hydrated");
    expect(child_span).not.toHaveClass("selected");

    // Capture & compare screenshot
    const results = await page.compareScreenshot();
    expect(results).toMatchScreenshot();
  });
});
