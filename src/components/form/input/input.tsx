import {
  Component,
  Host,
  h,
  Prop,
  State,
  Method,
  Element
} from "@stencil/core";

/**
 * Input Component.  Example Usage:
 * ```html
 * <uwe-input type="text" name="email" placeholder="Enter Email"></uwe-input>
 * ```
 */
@Component({
  tag: "uwe-input",
  styleUrl: "input.css",
  shadow: false
})
export class Input {
  /** @ignore This component */
  @Element() thisInput: HTMLElement;

  /** @ignore If input is in error state or not - red bordered */
  @State() isError: boolean = false;
  /** @ignore input value for form */
  @State() value: any;

  /**
   * ID of parent form
   * @category Optional
   */
  @Prop() form?: string;
  /**
   * ID of input
   * @category Optional
   */
  @Prop() inputId?: string;
  /**
   * Name of input for forms
   * @category Required
   */
  @Prop() name!: string;
  /**
   * Placeholder for input
   * @category Optional
   */
  @Prop() placeholder: string;
  /**
   * Type of input (text, email, password, etc.)
   * Default is 'text'
   * @category Optional
   */
  @Prop() type: string = "text";

  /**
   * Set error state to true to turn borde red
   * ```javascript
   * const uweInput = document.querySelector("uwe-input");
   * uweInput.error();
   * ```
   */
  @Method()
  async error() {
    this.isError = true;
  }

  /**
   * Set error state to false & reset value
   * @param errorState
   * @param valueState
   * ```javascript
   * const uweInput = document.querySelector("uwe-input");
   * // Reset error state and value
   * uweInput.reset();
   * // Reset only the value
   * uweInput.reset(false, true);
   * // Reset only error state
   * uweInput.reset(true, false);
   * ```
   */
  @Method()
  async reset(errorState: boolean = true, valueState: boolean = true) {
    if (errorState) this.isError = false;
    if (valueState) this.value = "";
  }

  /** @ignore */
  handleChange(event: any) {
    this.value = event.target.value;
  }

  /** @ignore */
  render() {
    return (
      <Host>
        <input
          type={this.type}
          id={this.inputId}
          value={this.value}
          name={this.name}
          form={this.form}
          placeholder={this.placeholder}
          onInput={event => this.handleChange(event)}
          class={
            "bg-gray-100 py-2 pl-2 border-2 focus:outline-none focus:bg-white" +
            (this.isError ? " error" : " border-gray-400")
          }
        />
      </Host>
    );
  }
}
