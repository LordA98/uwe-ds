import { Input } from "./input";

describe("uwe-input", () => {
  it("builds", () => {
    expect(new Input()).toBeTruthy();
  });

  describe("error", () => {
    it("puts select into error state", () => {
      const input = new Input();
      expect(input.isError).toBe(false);
      input.error();
      expect(input.isError).toBe(true);
    });

    it("leaves select in error state if already in error state", () => {
      const input = new Input();
      input.isError = true;
      expect(input.isError).toBe(true);
      input.error();
      expect(input.isError).toBe(true);
    });
  });

  describe("reset", () => {
    it("resets error state", () => {
      const input = new Input();
      input.isError = true;
      expect(input.isError).toBe(true);
      input.reset();
      expect(input.isError).toBe(false);
    });

    it("leaves error state false if already false", () => {
      const input = new Input();
      expect(input.isError).toBe(false);
      input.reset();
      expect(input.isError).toBe(false);
    });
  });
});
