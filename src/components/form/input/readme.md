# uwe-input

<!-- Auto Generated Below -->


## Properties

| Property            | Attribute     | Description                                                   | Type     | Default     |
| ------------------- | ------------- | ------------------------------------------------------------- | -------- | ----------- |
| `form`              | `form`        | ID of parent form                                             | `string` | `undefined` |
| `inputId`           | `input-id`    | ID of input                                                   | `string` | `undefined` |
| `name` _(required)_ | `name`        | Name of input for forms                                       | `string` | `undefined` |
| `placeholder`       | `placeholder` | Placeholder for input                                         | `string` | `undefined` |
| `type`              | `type`        | Type of input (text, email, password, etc.) Default is 'text' | `string` | `"text"`    |


## Methods

### `error() => Promise<void>`

Set error state to true to turn borde red
```javascript
const uweInput = document.querySelector("uwe-input");
uweInput.error();
```

#### Returns

Type: `Promise<void>`



### `reset(errorState?: boolean, valueState?: boolean) => Promise<void>`

Set error state to false & reset value

#### Returns

Type: `Promise<void>`




## Dependencies

### Used by

 - [uwe-page](../../page)

### Graph
```mermaid
graph TD;
  uwe-page --> uwe-input
  style uwe-input fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
