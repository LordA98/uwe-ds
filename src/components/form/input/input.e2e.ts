import { newE2EPage } from "@stencil/core/testing";

describe("uwe-input", () => {
  describe("type: text", () => {
    /**
     * Standard E2E & Screenshot Tests Integrated into Each Test
     */
    it("renders", async () => {
      const page = await newE2EPage();
      await page.setContent("<uwe-input type='text'></uwe-input>");

      const element = await page.find("uwe-input");
      expect(element).not.toBeNull();
      expect(element).toHaveClass("hydrated");
      expect(element).toEqualAttribute("type", "text");

      const childEl = await page.find("uwe-input input");
      expect(childEl).not.toBeNull();
      expect(childEl).toEqualAttribute("type", "text");
      expect(childEl).toHaveClass("border-gray-400");

      // Capture & compare screenshot
      const results = await page.compareScreenshot();
      expect(results).toMatchScreenshot();
    });

    it("responds to and renders on error", async () => {
      const page = await newE2EPage();
      await page.setContent("<uwe-input type='text'></uwe-input>");

      const element = await page.find("uwe-input");
      expect(element).not.toBeNull();
      expect(element).toHaveClass("hydrated");
      expect(element).toEqualAttribute("type", "text");

      await element.callMethod("error");
      await page.waitForChanges();

      const childEl = await page.find("uwe-input input");
      expect(childEl).not.toBeNull();
      expect(childEl).toEqualAttribute("type", "text");
      expect(childEl).toHaveClass("error");

      // Capture & compare screenshot
      const results = await page.compareScreenshot();
      expect(results).toMatchScreenshot();
    });

    it("responds to and renders on reset", async () => {
      const page = await newE2EPage();
      await page.setContent("<uwe-input type='text'></uwe-input>");

      const element = await page.find("uwe-input");
      expect(element).not.toBeNull();
      expect(element).toHaveClass("hydrated");
      expect(element).toEqualAttribute("type", "text");

      const childEl = await page.find("uwe-input input");
      expect(childEl).not.toBeNull();
      expect(childEl).toEqualAttribute("type", "text");

      // Check current value is empty & class is border-gray-400
      let curValue = await childEl.getProperty("value");
      expect(curValue).toBe("");
      expect(childEl).toHaveClass("border-gray-400");

      // Call error and set value
      await element.callMethod("error");
      await childEl.press("U");
      await childEl.press("W");
      await childEl.press("E");
      await page.waitForChanges();

      // Check value and check error class
      let val2 = await childEl.getProperty("value");
      expect(val2).toBe("UWE");
      expect(childEl).toHaveClass("error");

      // Call reset
      await element.callMethod("reset");
      await page.waitForChanges();

      // Check value is empty again & class is border-gray-400
      let resetValue = await childEl.getProperty("value");
      expect(resetValue).toBe("");
      expect(childEl).toHaveClass("border-gray-400");

      // Capture & compare screenshot
      const results = await page.compareScreenshot();
      expect(results).toMatchScreenshot();
    });
  });

  describe("type: password", () => {
    /**
     * Standard E2E & Screenshot Tests Integrated into Each Test
     */
    it("renders", async () => {
      const page = await newE2EPage();
      await page.setContent("<uwe-input type='password'></uwe-input>");

      const element = await page.find("uwe-input");
      expect(element).not.toBeNull();
      expect(element).toHaveClass("hydrated");
      expect(element).toEqualAttribute("type", "password");

      const childEl = await page.find("uwe-input input");
      expect(childEl).not.toBeNull();
      expect(childEl).toEqualAttribute("type", "password");
      expect(childEl).toHaveClass("border-gray-400");

      // Capture & compare screenshot
      const results = await page.compareScreenshot();
      expect(results).toMatchScreenshot();
    });

    it("responds to and renders on error", async () => {
      const page = await newE2EPage();
      await page.setContent("<uwe-input type='password'></uwe-input>");

      const element = await page.find("uwe-input");
      expect(element).not.toBeNull();
      expect(element).toHaveClass("hydrated");
      expect(element).toEqualAttribute("type", "password");

      await element.callMethod("error");
      await page.waitForChanges();

      const childEl = await page.find("uwe-input input");
      expect(childEl).not.toBeNull();
      expect(childEl).toEqualAttribute("type", "password");
      expect(childEl).toHaveClass("error");

      // Capture & compare screenshot
      const results = await page.compareScreenshot();
      expect(results).toMatchScreenshot();
    });

    it("responds to and renders on reset", async () => {
      const page = await newE2EPage();
      await page.setContent("<uwe-input type='password'></uwe-input>");

      const element = await page.find("uwe-input");
      expect(element).not.toBeNull();
      expect(element).toHaveClass("hydrated");
      expect(element).toEqualAttribute("type", "password");

      const childEl = await page.find("uwe-input input");
      expect(childEl).not.toBeNull();
      expect(childEl).toEqualAttribute("type", "password");

      // Check current value is empty & class is border-gray-400
      let curValue = await childEl.getProperty("value");
      expect(curValue).toBe("");
      expect(childEl).toHaveClass("border-gray-400");

      // Call error and set value
      await element.callMethod("error");
      await childEl.press("U");
      await childEl.press("W");
      await childEl.press("E");
      await page.waitForChanges();

      // Check value and check error class
      let val2 = await childEl.getProperty("value");
      expect(val2).toBe("UWE");
      expect(childEl).toHaveClass("error");

      // Call reset
      await element.callMethod("reset");
      await page.waitForChanges();

      // Check value is empty again & class is border-gray-400
      let resetValue = await childEl.getProperty("value");
      expect(resetValue).toBe("");
      expect(childEl).toHaveClass("border-gray-400");

      // Capture & compare screenshot
      const results = await page.compareScreenshot();
      expect(results).toMatchScreenshot();
    });
  });

  describe("type: checkbox", () => {
    /**
     * Standard E2E & Screenshot Tests Integrated into Each Test
     */
    it("renders", async () => {
      const page = await newE2EPage();
      await page.setContent("<uwe-input type='checkbox'></uwe-input>");

      const element = await page.find("uwe-input");
      expect(element).not.toBeNull();
      expect(element).toHaveClass("hydrated");
      expect(element).toEqualAttribute("type", "checkbox");

      const childEl = await page.find("uwe-input input");
      expect(childEl).not.toBeNull();
      expect(childEl).toEqualAttribute("type", "checkbox");
      expect(childEl).toHaveClass("border-gray-400");

      // Capture & compare screenshot
      const results = await page.compareScreenshot();
      expect(results).toMatchScreenshot();
    });

    it("responds to and renders on error", async () => {
      const page = await newE2EPage();
      await page.setContent("<uwe-input type='checkbox'></uwe-input>");

      const element = await page.find("uwe-input");
      expect(element).not.toBeNull();
      expect(element).toHaveClass("hydrated");
      expect(element).toEqualAttribute("type", "checkbox");

      await element.callMethod("error");
      await page.waitForChanges();

      const childEl = await page.find("uwe-input input");
      expect(childEl).not.toBeNull();
      expect(childEl).toEqualAttribute("type", "checkbox");
      expect(childEl).toHaveClass("error");

      // Capture & compare screenshot
      const results = await page.compareScreenshot();
      expect(results).toMatchScreenshot();
    });

    it("responds to and renders on reset", async () => {
      const page = await newE2EPage();
      await page.setContent("<uwe-input type='checkbox'></uwe-input>");

      const element = await page.find("uwe-input");
      expect(element).not.toBeNull();
      expect(element).toHaveClass("hydrated");
      expect(element).toEqualAttribute("type", "checkbox");

      const childEl = await page.find("uwe-input input");
      expect(childEl).not.toBeNull();
      expect(childEl).toEqualAttribute("type", "checkbox");

      // Check current value is empty & class is border-gray-400
      let curValue = await childEl.getAttribute("value");
      expect(curValue).toBe(null);
      expect(childEl).toHaveClass("border-gray-400");

      // Call error and set ticked
      await element.callMethod("error");
      await element.click();
      await page.waitForChanges();

      // Check ticked and check error class
      let val2 = await childEl.getAttribute("value");
      expect(val2).toBe("on");
      expect(childEl).toHaveClass("error");

      // Call reset
      await element.callMethod("reset");
      await page.waitForChanges();

      // Check value is empty again & class is border-gray-400
      let resetValue = await childEl.getAttribute("value");
      expect(resetValue).toBe("");
      expect(childEl).toHaveClass("border-gray-400");

      // Capture & compare screenshot
      const results = await page.compareScreenshot();
      expect(results).toMatchScreenshot();
    });
  });

  describe("type: radio", () => {
    /**
     * Standard E2E & Screenshot Tests Integrated into Each Test
     */
    it("renders", async () => {
      const page = await newE2EPage();
      await page.setContent("<uwe-input type='radio'></uwe-input>");

      const element = await page.find("uwe-input");
      expect(element).not.toBeNull();
      expect(element).toHaveClass("hydrated");
      expect(element).toEqualAttribute("type", "radio");

      const childEl = await page.find("uwe-input input");
      expect(childEl).not.toBeNull();
      expect(childEl).toEqualAttribute("type", "radio");
      expect(childEl).toHaveClass("border-gray-400");

      // Capture & compare screenshot
      const results = await page.compareScreenshot();
      expect(results).toMatchScreenshot();
    });

    it("responds to and renders on error", async () => {
      const page = await newE2EPage();
      await page.setContent("<uwe-input type='radio'></uwe-input>");

      const element = await page.find("uwe-input");
      expect(element).not.toBeNull();
      expect(element).toHaveClass("hydrated");
      expect(element).toEqualAttribute("type", "radio");

      await element.callMethod("error");
      await page.waitForChanges();

      const childEl = await page.find("uwe-input input");
      expect(childEl).not.toBeNull();
      expect(childEl).toEqualAttribute("type", "radio");
      expect(childEl).toHaveClass("error");

      // Capture & compare screenshot
      const results = await page.compareScreenshot();
      expect(results).toMatchScreenshot();
    });

    it("responds to and renders on reset", async () => {
      const page = await newE2EPage();
      await page.setContent("<uwe-input type='radio'></uwe-input>");

      const element = await page.find("uwe-input");
      expect(element).not.toBeNull();
      expect(element).toHaveClass("hydrated");
      expect(element).toEqualAttribute("type", "radio");

      const childEl = await page.find("uwe-input input");
      expect(childEl).not.toBeNull();
      expect(childEl).toEqualAttribute("type", "radio");

      // Check current value is empty & class is border-gray-400
      let curValue = await childEl.getAttribute("value");
      expect(curValue).toBe(null);
      expect(childEl).toHaveClass("border-gray-400");

      // Call error and set ticked
      await element.callMethod("error");
      await element.click();
      await page.waitForChanges();

      // Check ticked and check error class
      let val2 = await childEl.getAttribute("value");
      expect(val2).toBe("on");
      expect(childEl).toHaveClass("error");

      // Call reset
      await element.callMethod("reset");
      await page.waitForChanges();

      // Check value is empty again & class is border-gray-400
      let resetValue = await childEl.getAttribute("value");
      expect(resetValue).toBe("");
      expect(childEl).toHaveClass("border-gray-400");

      // Capture & compare screenshot
      const results = await page.compareScreenshot();
      expect(results).toMatchScreenshot();
    });
  });
});
