import { newE2EPage } from "@stencil/core/testing";

describe("uwe-form", () => {
  it("renders", async () => {
    const page = await newE2EPage();
    await page.setContent("<uwe-form></uwe-form>");

    const element = await page.find("uwe-form");
    expect(element).toHaveClass("hydrated");
  });
});
