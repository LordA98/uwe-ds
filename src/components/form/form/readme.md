# uwe-form

<!-- Auto Generated Below -->


## Properties

| Property | Attribute | Description                                | Type     | Default     |
| -------- | --------- | ------------------------------------------ | -------- | ----------- |
| `formId` | `form-id` | Form ID                                    | `string` | `undefined` |
| `method` | `method`  | HTTP Method used for form. Default is GET. | `string` | `"get"`     |


----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
