import { Component, h, Prop } from "@stencil/core";

/**
 * Form Component.  Example Usage:
 * ```html
 * <uwe-form method="get" id="form-id">
 *   <uwe-input-group class="uwe-row">
 *     <uwe-label class="uwe-col-2">Email:</uwe-label>
 *     <uwe-input type="text" name="email" placeholder="Enter Email"></uwe-input>
 *   </uwe-input-group>
 *
 *   <uwe-input-group class="uwe-row">
 *     <div class="uwe-col-2"></div> <!-- Offset -->
 *     <uwe-button type="submit" class="mr-2">Submit</uwe-button>
 *   </uwe-input-group>
 * </uwe-form>
 * ```
 */
@Component({
  tag: "uwe-form",
  styleUrl: "form.css",
  shadow: false
})
export class Form {
  /**
   * Form ID
   * @category Optional
   */
  @Prop() formId?: string;
  /**
   * HTTP Method used for form.
   * Default is GET.
   * @category Optional
   */
  @Prop() method?: string = "get";

  /** @ignore */
  render() {
    return (
      <form method={this.method} id={this.formId}>
        <slot />
      </form>
    );
  }
}
