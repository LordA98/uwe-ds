# uwe-textarea

<!-- Auto Generated Below -->


## Properties

| Property      | Attribute     | Description             | Type     | Default     |
| ------------- | ------------- | ----------------------- | -------- | ----------- |
| `name`        | `name`        | name of input for forms | `string` | `undefined` |
| `placeholder` | `placeholder` | Placeholder for input   | `string` | `undefined` |
| `textareaId`  | `textarea-id` | id of input             | `string` | `undefined` |


## Methods

### `error() => Promise<void>`

Set error state to true to turn borde red

#### Returns

Type: `Promise<void>`



### `reset(errorState?: boolean, valueState?: boolean) => Promise<void>`

Set error state to false & reset value

#### Returns

Type: `Promise<void>`




## Dependencies

### Used by

 - [uwe-page](../../page)

### Graph
```mermaid
graph TD;
  uwe-page --> uwe-textarea
  style uwe-textarea fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
