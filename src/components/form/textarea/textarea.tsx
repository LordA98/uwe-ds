import {
  Component,
  Host,
  h,
  Method,
  Element,
  Prop,
  State
} from "@stencil/core";

@Component({
  tag: "uwe-textarea",
  styleUrl: "textarea.css",
  shadow: true
})
export class Textarea {
  /** This component */
  @Element() thisInput: HTMLElement;

  /** If input is in error state or not - red bordered */
  @State() isError: boolean = false;
  /** input value for form */
  @State() value: any;

  /** name of input for forms */
  @Prop() name: string;
  /** Placeholder for input */
  @Prop() placeholder: string;
  /** id of input */
  @Prop() textareaId: string;

  /**
   * Set error state to true to turn borde red
   */
  @Method()
  async error() {
    this.isError = true;
  }

  /**
   * Set error state to false & reset value
   * @param errorState
   * @param valueState
   */
  @Method()
  async reset(errorState: boolean = true, valueState: boolean = true) {
    if (errorState) this.isError = false;
    if (valueState) this.value = "";
  }

  handleChange(event: any) {
    this.value = event.target.value;
  }

  render() {
    return (
      <Host>
        <textarea
          rows={4}
          cols={40}
          id={this.textareaId}
          value={this.value}
          name={this.name}
          placeholder={this.placeholder}
          onInput={event => this.handleChange(event)}
          class={this.isError ? "error" : ""}
        />
      </Host>
    );
  }
}
