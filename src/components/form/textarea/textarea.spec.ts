import { Textarea } from "./textarea";

describe("uwe-textarea", () => {
  it("builds", () => {
    expect(new Textarea()).toBeTruthy();
  });

  describe("error", () => {
    it("puts textarea into error state", () => {
      const textarea = new Textarea();
      expect(textarea.isError).toBe(false);
      textarea.error();
      expect(textarea.isError).toBe(true);
    });

    it("leaves textarea in error state if already in error state", () => {
      const textarea = new Textarea();
      textarea.isError = true;
      expect(textarea.isError).toBe(true);
      textarea.error();
      expect(textarea.isError).toBe(true);
    });
  });

  describe("reset", () => {
    it("resets error state", () => {
      const textarea = new Textarea();
      textarea.isError = true;
      expect(textarea.isError).toBe(true);
      textarea.reset();
      expect(textarea.isError).toBe(false);
    });

    it("leaves error state false if already false", () => {
      const textarea = new Textarea();
      expect(textarea.isError).toBe(false);
      textarea.reset();
      expect(textarea.isError).toBe(false);
    });
  });
});
