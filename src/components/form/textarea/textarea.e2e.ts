import { newE2EPage } from "@stencil/core/testing";

describe("uwe-textarea", () => {
  /**
   * Standard E2E Tests
   */
  it("renders", async () => {
    const page = await newE2EPage();
    await page.setContent("<uwe-textarea></uwe-textarea>");

    const element = await page.find("uwe-textarea");
    expect(element).not.toBeNull();
    expect(element).toHaveClass("hydrated");
  });

  /**
   * Screenshot Tests
   */
  it("renders the textarea correctly", async () => {
    const page = await newE2EPage();

    // Link to global CSS & render select w/ option
    await page.setContent(
      `<link href='http://localhost:3333/build/uwe-ds.css' rel='stylesheet' />
      <uwe-textarea textareaId="textarea-test" name="test" placeholder="Textarea"></uwe-textarea>`
    );

    const textarea = await page.find("uwe-textarea");
    expect(textarea).toHaveClass("hydrated");

    // Capture & compare screenshot
    const results = await page.compareScreenshot();
    expect(results).toMatchScreenshot();
  });

  it("responds to and renders in active / focus state", async () => {
    const page = await newE2EPage();

    // Link to global CSS & render select w/ option
    await page.setContent(
      `<link href='http://localhost:3333/build/uwe-ds.css' rel='stylesheet' />
      <uwe-textarea textareaId="textarea-test" name="test" placeholder="Textarea"></uwe-textarea>`
    );

    const textarea = await page.find("uwe-textarea");
    expect(textarea).toHaveClass("hydrated");

    await textarea.click();
    await page.waitForChanges();

    // Capture & compare screenshot
    const results = await page.compareScreenshot();
    expect(results).toMatchScreenshot();
  });
});
