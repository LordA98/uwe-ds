import { Component, Host, h, Prop } from "@stencil/core";

/**
 * Label Component.  Example Usage:
 * ```html
 * <uwe-label for="inputId" class="uwe-col-2">Email:</uwe-label>
 * ```
 */
@Component({
  tag: "uwe-label",
  styleUrl: "label.css",
  shadow: true
})
export class Label {
  /**
   * ID of Input this label corresponds to.
   * @category Required
   */
  @Prop() for!: string;
  /**
   * Whether the input is required
   * Default = false.
   * @category Optional
   */
  @Prop() required?: boolean = false;

  /** @ignore */
  render() {
    return (
      <Host class="pr-3 my-auto text-right">
        <label htmlFor={this.for}>
          <slot />
          {this.required ? <span class="required">*</span> : ""}
        </label>
      </Host>
    );
  }
}
