import { newE2EPage } from "@stencil/core/testing";

describe("uwe-label", () => {
  /**
   * Standard E2E & Screenshot Tests
   */
  it("renders", async () => {
    const page = await newE2EPage();
    await page.setContent("<uwe-label>Label</uwe-label>");

    const element = await page.find("uwe-label");
    expect(element).not.toBeNull();
    expect(element).toHaveClass("hydrated");

    // Capture & compare screenshot
    const results = await page.compareScreenshot();
    expect(results).toMatchScreenshot();
  });

  it("renders with required", async () => {
    const page = await newE2EPage();
    await page.setContent("<uwe-label required>Label</uwe-label>");

    const element = await page.find("uwe-label");
    expect(element).not.toBeNull();
    expect(element).toHaveClass("hydrated");
    expect(element).toHaveAttribute("required");

    const asterisk = await page.find("uwe-label >>> span");
    expect(asterisk).not.toBeNull();
    expect(asterisk).toHaveClass("required");

    // Capture & compare screenshot
    const results = await page.compareScreenshot();
    expect(results).toMatchScreenshot();
  });
});
