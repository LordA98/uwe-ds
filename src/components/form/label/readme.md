# uwe-label

<!-- Auto Generated Below -->


## Properties

| Property           | Attribute  | Description                                    | Type      | Default     |
| ------------------ | ---------- | ---------------------------------------------- | --------- | ----------- |
| `for` _(required)_ | `for`      | ID of Input this label corresponds to.         | `string`  | `undefined` |
| `required`         | `required` | Whether the input is required Default = false. | `boolean` | `false`     |


## Dependencies

### Used by

 - [uwe-page](../../page)

### Graph
```mermaid
graph TD;
  uwe-page --> uwe-label
  style uwe-label fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
