import { Component, Host, h, Element } from "@stencil/core";

/**
 * Input Group Component.  Example Usage:
 * ```html
 * <uwe-input-group class="uwe-row">
 *   <uwe-label class="uwe-col-2">Email:</uwe-label>
 *   <uwe-input type="text" name="email" placeholder="Enter Email"></uwe-input>
 * </uwe-input-group>
 * ```
 */
@Component({
  tag: "uwe-input-group",
  styleUrl: "input-group.css",
  shadow: true
})
export class InputGroup {
  /** @ignore This input group element */
  @Element() thisInputGroup: HTMLElement;

  /** @ignore */
  render() {
    return (
      <Host class="m-3">
        <slot />
      </Host>
    );
  }
}
