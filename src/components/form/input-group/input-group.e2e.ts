import { newE2EPage } from "@stencil/core/testing";

describe("uwe-input-group", () => {
  it("renders", async () => {
    const page = await newE2EPage();
    await page.setContent("<uwe-input-group></uwe-input-group>");

    const element = await page.find("uwe-input-group");
    expect(element).toHaveClass("hydrated");
  });
});
