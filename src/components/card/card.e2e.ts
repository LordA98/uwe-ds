import { newE2EPage } from "@stencil/core/testing";

describe("uwe-card", () => {
  /**
   * Standard E2E Tests
   */
  it("renders", async () => {
    const page = await newE2EPage();
    await page.setContent("<uwe-card></uwe-card>");

    const element = await page.find("uwe-card");
    expect(element).toHaveClass("hydrated");
  });

  it("renders the image", async () => {
    const page = await newE2EPage();
    await page.setContent(`
      <uwe-card>
        <img
          src="https://www.uwe.ac.uk/externalHome/img/ug-home.jpg"
          alt="UWE Stock Image"
          slot="card-image"
        />
      </uwe-card>
    `);

    const card = await page.find("uwe-card");
    expect(card).toHaveClass("hydrated");

    const img = await page.find("uwe-card img");
    expect(img).not.toBeNull();

    const title_container = await page.find(
      "uwe-card >>> slot[name=card-image]"
    );
    expect(title_container).not.toBeNull();
  });

  it("renders the title", async () => {
    const page = await newE2EPage();
    await page.setContent(`
      <uwe-card>
        <uwe-heading size="3" slot="card-title">Undergraduate Study</uwe-heading>
      </uwe-card>
    `);

    const card = await page.find("uwe-card");
    expect(card).toHaveClass("hydrated");

    const title = await page.find("uwe-card uwe-heading");
    expect(title).not.toBeNull();

    const title_container = await page.find(
      "uwe-card >>> slot[name=card-title]"
    );
    expect(title_container).not.toBeNull();
  });

  it("renders the text", async () => {
    const page = await newE2EPage();
    await page.setContent(`
      <uwe-card>
        <uwe-paragraph slot="card-text">
          Undergraduate study guide to UWE Bristol including courses, student
          life, fees and funding, accommodation and student support.
        </uwe-paragraph>
      </uwe-card>
    `);

    const card = await page.find("uwe-card");
    expect(card).toHaveClass("hydrated");

    const paragraph = await page.find("uwe-card uwe-paragraph");
    expect(paragraph).not.toBeNull();

    const text_container = await page.find("uwe-card >>> slot[name=card-text]");
    expect(text_container).not.toBeNull();
  });

  it("renders a single button", async () => {
    const page = await newE2EPage();
    await page.setContent(`
      <uwe-card>
      <div slot="card-button">
        <uwe-button>Undergraduate</uwe-button>
      </div>
      </uwe-card>
    `);

    // Card
    const card = await page.find("uwe-card");
    expect(card).toHaveClass("hydrated");

    // Button in standard DOM
    const buttonDiv = await page.find("uwe-card div[slot=card-button]");
    expect(buttonDiv).not.toBeNull();

    const buttons = await buttonDiv.findAll("uwe-button");
    expect(buttons.length).toEqual(1);

    // Button in shadow DOM
    const shadowButtonDiv = await page.find("uwe-card >>> .button-container");
    expect(shadowButtonDiv).not.toBeNull();

    const shadowButtons = await shadowButtonDiv.findAll("uwe-button");
    expect(shadowButtons.length).toEqual(1);
  });

  it("renders two buttons", async () => {
    const page = await newE2EPage();
    await page.setContent(`
      <uwe-card>
      <div slot="card-button">
        <uwe-button>Undergraduate</uwe-button>
        <uwe-button type="reset">Postgraduate</uwe-button>
      </div>
      </uwe-card>
    `);

    // Card
    const card = await page.find("uwe-card");
    expect(card).toHaveClass("hydrated");

    // Button in standard DOM
    const buttonDiv = await page.find("uwe-card div[slot=card-button]");
    expect(buttonDiv).not.toBeNull();

    const buttons = await buttonDiv.findAll("uwe-button");
    expect(buttons.length).toEqual(2);

    // Button in shadow DOM
    const shadowButtonDiv = await page.find("uwe-card >>> .button-container");
    expect(shadowButtonDiv).not.toBeNull();

    const shadowButtons = await shadowButtonDiv.findAll("uwe-button");
    expect(shadowButtons.length).toEqual(2);
  });

  /**
   * Screenshot Tests
   */
  it("renders card with image", async () => {
    const page = await newE2EPage();

    // Link to global CSS
    await page.setContent(`
      <link href="http://localhost:3333/build/uwe-ds.css" rel="stylesheet" />
      <uwe-card>
        <img
          src="https://www.uwe.ac.uk/externalHome/img/ug-home.jpg"
          alt="UWE Stock Image"
          slot="card-image"
        />
      </uwe-card>  
    `);

    const element = await page.find("uwe-card");
    expect(element).toHaveClass("hydrated");

    const img = await page.find("uwe-card img");
    expect(img).not.toBeNull();

    const title_container = await page.find(
      "uwe-card >>> slot[name=card-image]"
    );
    expect(title_container).not.toBeNull();

    // Capture & compare screenshot
    const results = await page.compareScreenshot();
    expect(results).toMatchScreenshot();
  });

  it("renders card with title", async () => {
    const page = await newE2EPage();

    // Link to global CSS
    await page.setContent(`
      <link href="http://localhost:3333/build/uwe-ds.css" rel="stylesheet" />
      <uwe-card>
        <uwe-heading size="3" slot="card-title">Undergraduate Study</uwe-heading>
      </uwe-card>
    `);

    const card = await page.find("uwe-card");
    expect(card).toHaveClass("hydrated");

    const title = await page.find("uwe-card uwe-heading");
    expect(title).not.toBeNull();

    const title_container = await page.find(
      "uwe-card >>> slot[name=card-title]"
    );
    expect(title_container).not.toBeNull();

    // Capture & compare screenshot
    const results = await page.compareScreenshot();
    expect(results).toMatchScreenshot();
  });

  it("renders card with text", async () => {
    const page = await newE2EPage();

    // Link to global CSS
    await page.setContent(`
      <link href="http://localhost:3333/build/uwe-ds.css" rel="stylesheet" />
      <uwe-card>
        <uwe-paragraph slot="card-text">
          Undergraduate study guide to UWE Bristol including courses, student
          life, fees and funding, accommodation and student support.
        </uwe-paragraph>
      </uwe-card>
    `);

    const card = await page.find("uwe-card");
    expect(card).toHaveClass("hydrated");

    const paragraph = await page.find("uwe-card uwe-paragraph");
    expect(paragraph).not.toBeNull();

    const text_container = await page.find("uwe-card >>> slot[name=card-text]");
    expect(text_container).not.toBeNull();

    // Capture & compare screenshot
    const results = await page.compareScreenshot();
    expect(results).toMatchScreenshot();
  });

  it("renders card with single button", async () => {
    const page = await newE2EPage();

    // Link to global CSS
    await page.setContent(`
      <link href="http://localhost:3333/build/uwe-ds.css" rel="stylesheet" />
      <uwe-card>
        <div slot="card-button">
          <uwe-button>Undergraduate</uwe-button>
        </div>
      </uwe-card>
    `);

    // Card
    const card = await page.find("uwe-card");
    expect(card).toHaveClass("hydrated");

    // Button in standard DOM
    const buttonDiv = await page.find("uwe-card div[slot=card-button]");
    expect(buttonDiv).not.toBeNull();

    const buttons = await buttonDiv.findAll("uwe-button");
    expect(buttons.length).toEqual(1);

    // Button in shadow DOM
    const shadowButtonDiv = await page.find("uwe-card >>> .button-container");
    expect(shadowButtonDiv).not.toBeNull();

    const shadowButtons = await shadowButtonDiv.findAll("uwe-button");
    expect(shadowButtons.length).toEqual(1);

    // Capture & compare screenshot
    const results = await page.compareScreenshot();
    expect(results).toMatchScreenshot();
  });

  it("renders card two buttons", async () => {
    const page = await newE2EPage();

    // Link to global CSS
    await page.setContent(`
      <link href="http://localhost:3333/build/uwe-ds.css" rel="stylesheet" />
      <uwe-card>
        <div slot="card-button">
          <uwe-button>Undergraduate</uwe-button>
          <uwe-button type="reset">Postgraduate</uwe-button>
        </div>
      </uwe-card>
    `);

    // Card
    const card = await page.find("uwe-card");
    expect(card).toHaveClass("hydrated");

    // Button in standard DOM
    const buttonDiv = await page.find("uwe-card div[slot=card-button]");
    expect(buttonDiv).not.toBeNull();

    const buttons = await buttonDiv.findAll("uwe-button");
    expect(buttons.length).toEqual(2);

    // Button in shadow DOM
    const shadowButtonDiv = await page.find("uwe-card >>> .button-container");
    expect(shadowButtonDiv).not.toBeNull();

    const shadowButtons = await shadowButtonDiv.findAll("uwe-button");
    expect(shadowButtons.length).toEqual(2);

    // Capture & compare screenshot
    const results = await page.compareScreenshot();
    expect(results).toMatchScreenshot();
  });

  it("renders a whole card", async () => {
    const page = await newE2EPage();

    // Link to global CSS
    await page.setContent(`
      <link href="http://localhost:3333/build/uwe-ds.css" rel="stylesheet" />
      <uwe-card>
        <img
          src="https://www.uwe.ac.uk/externalHome/img/ug-home.jpg"
          alt="UWE Stock Image"
          slot="card-image"
        />
        <uwe-heading size="3" slot="card-title">Undergraduate Study</uwe-heading>
        <uwe-paragraph slot="card-text">
          Undergraduate study guide to UWE Bristol including courses, student
          life, fees and funding, accommodation and student support.
        </uwe-paragraph>
        <div slot="card-button">
          <uwe-button>Undergraduate</uwe-button>
          <uwe-button type="reset">Postgraduate</uwe-button>
        </div>
      </uwe-card>
    `);

    // Card
    const card = await page.find("uwe-card");
    expect(card).toHaveClass("hydrated");

    // Capture & compare screenshot
    const results = await page.compareScreenshot();
    expect(results).toMatchScreenshot();
  });
});
