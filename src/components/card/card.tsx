import { Component, Host, h, Element } from "@stencil/core";

/**
 * Card Component.  Example Usage:
 * ```html
 * <uwe-card>
 *   <img
 *     src="https://www.uwe.ac.uk/externalHome/img/ug-home.jpg"
 *     alt="UWE Stock"
 *     slot="card-image"
 *   />
 *   <uwe-heading size="3" slot="card-title">Undergraduate Study</uwe-heading>
 *   <uwe-paragraph slot="card-text">
 *     Undergraduate study guide to UWE Bristol including courses, student
 *    life, fees and funding, accommodation and student support.
 *       </uwe-paragraph>
 *   <div slot="card-button">
 *     <uwe-button>Undergraduate</uwe-button>
 *     <uwe-button type="reset">Postgraduate</uwe-button>
 *   </div>
 * </uwe-card>
 * ```
 */
@Component({
  tag: "uwe-card",
  styleUrls: ["card.css", "../../global/global.css"],
  shadow: true
})
export class Card {
  /** @ignore This card element */
  @Element() thisCard: HTMLElement;

  /** @ignore */
  render() {
    return (
      <Host class="inline-block shadow-md m-3 text-center">
        <div class="image-container">
          <slot name="card-image" />
        </div>
        <div class="p-2">
          <div class="text-container title">
            <slot name="card-title" />
          </div>
          <div class="text-container text">
            <slot name="card-text" />
          </div>
          <div class="text-container button">
            <div class="button-container ">
              {/* Print buttons using array so that they conform to the justify-around styling */}
              {this.thisCard.querySelector('[slot="card-button"]')
                ? Array.from(
                    this.thisCard.querySelector('[slot="card-button"]').children
                  ).map(child => <span innerHTML={child.outerHTML} />)
                : ""}
            </div>
          </div>
        </div>
      </Host>
    );
  }
}
