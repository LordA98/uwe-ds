# uwe-image

<!-- Auto Generated Below -->


## Properties

| Property | Attribute | Description                | Type     | Default     |
| -------- | --------- | -------------------------- | -------- | ----------- |
| `alt`    | `alt`     | Alternative text for image | `string` | `undefined` |
| `height` | `height`  | Height                     | `string` | `undefined` |
| `src`    | `src`     | Image source URL           | `string` | `undefined` |
| `width`  | `width`   | Width                      | `string` | `undefined` |


## Dependencies

### Used by

 - [uwe-logo](../logo)
 - [uwe-select](../form/select)

### Graph
```mermaid
graph TD;
  uwe-logo --> uwe-image
  uwe-select --> uwe-image
  style uwe-image fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
