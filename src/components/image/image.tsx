import { Component, h, Prop } from "@stencil/core";

@Component({
  tag: "uwe-image",
  styleUrl: "image.css",
  shadow: true
})
export class Image {
  /** Alternative text for image */
  @Prop() alt: string;
  /** Height */
  @Prop() height?: string;
  /** Image source URL */
  @Prop() src: string;
  /** Width */
  @Prop() width?: string;

  render() {
    return (
      <img
        src={this.src}
        alt={this.alt}
        height={this.height}
        width={this.width}
      />
    );
  }
}
