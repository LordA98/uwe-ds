import { newE2EPage } from "@stencil/core/testing";

describe("uwe-image", () => {
  it("renders", async () => {
    const page = await newE2EPage();
    await page.setContent("<uwe-image></uwe-image>");

    const element = await page.find("uwe-image");
    expect(element).toHaveClass("hydrated");
  });
});
