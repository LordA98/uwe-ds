# uwe-button

<!-- Auto Generated Below -->


## Properties

| Property   | Attribute  | Description                                                                                 | Type                                                         | Default             |
| ---------- | ---------- | ------------------------------------------------------------------------------------------- | ------------------------------------------------------------ | ------------------- |
| `disabled` | `disabled` | If true, the user cannot interact with the button. Defaults to `false`.                     | `boolean`                                                    | `false`             |
| `form`     | `form`     | Form that this button belongs to                                                            | `string`                                                     | `undefined`         |
| `name`     | `name`     | Name of button if used in form.                                                             | `string`                                                     | `undefined`         |
| `type`     | `type`     | The type of the button. Options: `"submit"`, `"reset"`, and `"button"`. Default: `"button"` | `ButtonType.button \| ButtonType.reset \| ButtonType.submit` | `ButtonType.button` |
| `value`    | `value`    | The text value of the button if used in a form.                                             | `string`                                                     | `undefined`         |


## Methods

### `disable() => Promise<void>`

Disable the button
```javascript
const uweButton = document.querySelector("uwe-button");
uweButton.disable();
```

#### Returns

Type: `Promise<void>`



### `enable() => Promise<void>`

Enable the button
```javascript
const uweButton = document.querySelector("uwe-button");
uweButton.enable();
```

#### Returns

Type: `Promise<void>`




## Dependencies

### Used by

 - [uwe-page](../page)

### Graph
```mermaid
graph TD;
  uwe-page --> uwe-button
  style uwe-button fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
