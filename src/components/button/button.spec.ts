import { Button } from "./button";

describe("uwe-button", () => {
  it("builds", () => {
    expect(new Button()).toBeTruthy();
  });

  describe("disable", () => {
    it("disables the button", () => {
      const button = new Button();
      expect(button.disabled).toBe(false);
      button.disable();
      expect(button.disabled).toBe(true);
    });

    it("leaves the button disabled if already disabled", () => {
      const button = new Button();
      button.disabled = true;
      expect(button.disabled).toBe(true);
      button.disable();
      expect(button.disabled).toBe(true);
    });
  });

  describe("enable", () => {
    it("enables the button", () => {
      const button = new Button();
      button.disabled = true;
      expect(button.disabled).toBe(true);
      button.enable();
      expect(button.disabled).toBe(false);
    });

    it("leaves the button enabled if already enabled", () => {
      const button = new Button();
      expect(button.disabled).toBe(false);
      button.enable();
      expect(button.disabled).toBe(false);
    });
  });
});
