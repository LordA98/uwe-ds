import { Component, h, Prop, Element, Method } from "@stencil/core";
import { ButtonType } from "../../utils/utils";

/**
 * Button Component.  Example usage:
 * ```html
 * <uwe-button>Basic Button</uwe-button>
 * <uwe-button disabled>Disabled Button</uwe-button>
 * <uwe-button type="submit">Submit Button</uwe-button>
 * <uwe-button type="reset">Reset Button</uwe-button>
 * <uwe-button type="submit" form="formId" name="submitButton">Submit Button for Form</uwe-button>
 * <uwe-button type="reset" form="formId" name="resetButton">Reset Button for Form</uwe-button>
 * <uwe-button type="submit" form="formId" name="submitButton" disabled>Disabled Submit Button for Form</uwe-button>
 * ```
 */
@Component({
  tag: "uwe-button",
  styleUrl: "button.css",
  scoped: true
})
export class Button {
  /** @ignore This 'uwe-button' element */
  @Element() thisEl: HTMLElement;

  /**
   * If true, the user cannot interact with the button. Defaults to `false`.
   * @category Optional
   */
  @Prop() disabled?: boolean = false;
  /**
   * Form that this button belongs to
   * @category Optional
   */
  @Prop() form?: string;
  /**
   * Name of button if used in form.
   * @category Optional
   */
  @Prop() name?: string;
  /**
   * The type of the button.
   * Options: `"submit"`, `"reset"`, and `"button"`.
   * Default: `"button"`
   * @category Optional
   */
  @Prop() type?: ButtonType = ButtonType.button;
  /**
   * The text value of the button if used in a form.
   * @category Optional
   */
  @Prop() value?: string;

  /**
   * Disable the button
   * ```javascript
   * const uweButton = document.querySelector("uwe-button");
   * uweButton.disable();
   * ```
   */
  @Method()
  async disable() {
    this.disabled = true;
  }

  /**
   * Enable the button
   * ```javascript
   * const uweButton = document.querySelector("uwe-button");
   * uweButton.enable();
   * ```
   */
  @Method()
  async enable() {
    this.disabled = false;
  }

  /** @ignore */
  componentWillRender() {
    // Set type to "button" if invalid type given
    if (!(this.type in ButtonType)) {
      this.type = ButtonType.button;
    }
  }

  /** @ignore */
  render() {
    return (
      <button
        type={this.type}
        disabled={this.disabled}
        value={this.value}
        name={this.name}
        form={this.form}
        class="uwe-button"
      >
        <slot />
      </button>
    );
  }
}
