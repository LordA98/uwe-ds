import { newE2EPage } from "@stencil/core/testing";

describe("uwe-button", () => {
  /**
   * Standard E2E Tests
   */
  it("renders", async () => {
    const page = await newE2EPage();
    await page.setContent("<uwe-button></uwe-button>");

    const element = await page.find("uwe-button");
    expect(element).not.toBeNull();
    expect(element).toHaveClass("hydrated");
  });

  it("displays correct text content", async () => {
    const page = await newE2EPage();
    await page.setContent("<uwe-button>Open Days</uwe-button>");

    const element = await page.find("uwe-button");
    expect(element).toEqualText("Open Days");
  });

  /**
   * Screenshot Tests
   */
  it("renders button type correctly", async () => {
    const page = await newE2EPage();

    // Link to global CSS
    await page.setContent(
      '<link href="http://localhost:3333/build/uwe-ds.css" rel="stylesheet" /><uwe-button type="button">Open Days</uwe-button>'
    );

    const element = await page.find("uwe-button");
    expect(element).toHaveClass("hydrated");
    expect(element).toEqualAttribute("type", "button");

    // Capture & compare screenshot
    const results = await page.compareScreenshot();
    expect(results).toMatchScreenshot();
  });

  it("renders reset type correctly", async () => {
    const page = await newE2EPage();

    // Link to global CSS
    await page.setContent(
      '<link href="http://localhost:3333/build/uwe-ds.css" rel="stylesheet" /><uwe-button type="reset">Open Days</uwe-button>'
    );

    const element = await page.find("uwe-button");
    expect(element).toHaveClass("hydrated");
    expect(element).toEqualAttribute("type", "reset");

    // Capture & compare screenshot
    const results = await page.compareScreenshot();
    expect(results).toMatchScreenshot();
  });

  it("renders and responds to disabled property", async () => {
    const page = await newE2EPage();

    // Link to global CSS
    await page.setContent(
      '<link href="http://localhost:3333/build/uwe-ds.css" rel="stylesheet" /><uwe-button disabled>Open Days</uwe-button>'
    );

    const element = await page.find("uwe-button");
    expect(element).toHaveAttribute("disabled");

    // Capture & compare screenshot
    const results = await page.compareScreenshot();
    expect(results).toMatchScreenshot();
  });
});
