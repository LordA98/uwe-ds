/**
 * Functions that return each UWE-DS Component
 * Called from page.tsx - to reduce / tidy up code in that file.
 */
import { h } from "@stencil/core";
import { ButtonType } from "../../utils/utils";

/** @ignore Button */
export function button(
  value: string,
  type: ButtonType,
  name: string,
  disabled: boolean
): HTMLElement {
  return (
    <uwe-button value={value} type={type} name={name} disabled={disabled}>
      Hello
    </uwe-button>
  );
}

/** @ignore Paragraph */
export function paragraph(): HTMLElement {
  return <uwe-paragraph>Hello, this a paragraph.</uwe-paragraph>;
}

/** @ignore Link */
export function link(
  href: string,
  target: string,
  tooltip: string
): HTMLElement {
  return (
    <uwe-link href={href} target={target} tooltip={tooltip}>
      Click Me
    </uwe-link>
  );
}

/** @ignore Heading */
export function heading(size: number): HTMLElement {
  return <uwe-heading size={size}>Heading</uwe-heading>;
}

/** @ignore Logo */
export function logo(
  image: string,
  alt: string,
  height: string,
  width: string
) {
  return (
    <uwe-logo image={image} alt={alt} height={height} width={width}></uwe-logo>
  );
}

/** @ignore Card */
export function card(): HTMLElement {
  return (
    <uwe-card>
      <img
        src="https://www.uwe.ac.uk/externalHome/img/ug-home.jpg"
        alt="UWE Stock Image"
        slot="card-image"
      />
      <uwe-heading size={3} slot="card-title">
        Undergraduate Study
      </uwe-heading>
      <uwe-paragraph slot="card-text">
        Undergraduate study guide to UWE Bristol including courses, student
        life, fees and funding, accommodation and student support.
      </uwe-paragraph>
      <div slot="card-button">
        <uwe-button>Undergraduate</uwe-button>
        <uwe-button type={ButtonType.reset}>Postgraduate</uwe-button>
      </div>
    </uwe-card>
  );
}

/** @ignore Label */
export function label(forInput: string, required: boolean): HTMLElement {
  return (
    <uwe-label for={forInput} required={required}>
      Label
    </uwe-label>
  );
}

/** @ignore Textarea */
export function textarea(): HTMLElement {
  return <uwe-textarea placeholder="Textarea Placeholder"></uwe-textarea>;
}

/** @ignore Select  FIXME: */
export function select(name: string): HTMLElement {
  return (
    <uwe-select name={name}>
      <uwe-option value="1" selected>
        Opt 1
      </uwe-option>
      <uwe-option value="2">Opt 2</uwe-option>
    </uwe-select>
  );
}

/** @ignore Input */
export function input(
  name: string,
  type: string,
  placeholder: any
): HTMLElement {
  return (
    <uwe-input name={name} type={type} placeholder={placeholder}></uwe-input>
  );
}
