# uwe-page

<!-- Auto Generated Below -->


## Properties

| Property  | Attribute | Description | Type            | Default     |
| --------- | --------- | ----------- | --------------- | ----------- |
| `history` | --        |             | `RouterHistory` | `undefined` |
| `match`   | --        |             | `MatchResults`  | `undefined` |


## Dependencies

### Depends on

- [uwe-button](../button)
- [uwe-paragraph](../paragraph)
- [uwe-link](../link)
- [uwe-heading](../heading)
- [uwe-logo](../logo)
- [uwe-card](../card)
- [uwe-heading](../heading)
- [uwe-paragraph](../paragraph)
- [uwe-button](../button)
- [uwe-button](../button)
- [uwe-label](../form/label)
- [uwe-textarea](../form/textarea)
- [uwe-select](../form/select)
- [uwe-option](../form/option)
- [uwe-option](../form/option)
- [uwe-input](../form/input)

### Graph
```mermaid
graph TD;
  uwe-page --> uwe-button
  uwe-page --> uwe-paragraph
  uwe-page --> uwe-link
  uwe-page --> uwe-heading
  uwe-page --> uwe-logo
  uwe-page --> uwe-card
  uwe-page --> uwe-heading
  uwe-page --> uwe-paragraph
  uwe-page --> uwe-button
  uwe-page --> uwe-button
  uwe-page --> uwe-label
  uwe-page --> uwe-textarea
  uwe-page --> uwe-select
  uwe-page --> uwe-option
  uwe-page --> uwe-option
  uwe-page --> uwe-input
  uwe-logo --> uwe-image
  uwe-select --> uwe-image
  style uwe-page fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
