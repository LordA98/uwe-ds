import { Component, h, Prop } from "@stencil/core";
import { MatchResults, RouterHistory } from "@stencil/router";
import * as Components from "./components";

/** @ignore */
@Component({
  tag: "uwe-page",
  styleUrl: "page.css",
  shadow: true
})
export class Page {
  /** @ignore HTMLElement to hold uwe-ds component specified in URL */
  component: HTMLElement;

  /** @ignore Stencil Router URL */
  @Prop() history: RouterHistory;
  @Prop() match: MatchResults;

  /** @ignore */
  componentWillLoad() {
    switch (this.match.params.component) {
      case "button":
        this.component = Components.button(
          this.history.location.query.value,
          this.history.location.query.type,
          this.history.location.query.name,
          this.history.location.query.disabled
        );
        break;
      case "paragraph":
        this.component = Components.paragraph();
        break;
      case "link":
        this.component = Components.link(
          this.history.location.query.href,
          this.history.location.query.target,
          this.history.location.query.tooltip
        );
        break;
      case "heading":
        this.component = Components.heading(this.history.location.query.size);
        break;
      case "logo":
        this.component = Components.logo(
          this.history.location.query.image,
          this.history.location.query.alt,
          this.history.location.query.height,
          this.history.location.query.width
        );
        break;
      case "card":
        this.component = Components.card();
        break;
      case "label":
        this.component = Components.label(
          this.history.location.query.for,
          this.history.location.query.required
        );
        break;
      case "textarea":
        this.component = Components.textarea();
        break;
      case "select":
        this.component = Components.select(this.history.location.query.name);
        break;
      case "input":
        this.component = Components.input(
          this.history.location.query.name,
          this.history.location.query.type,
          this.history.location.query.placeholder
        );
        break;
    }
  }

  /** @ignore */
  render() {
    return <div>{this.component}</div>;
  }
}
