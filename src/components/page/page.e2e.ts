import { newE2EPage } from "@stencil/core/testing";

describe("uwe-page", () => {
  it("renders", async () => {
    const page = await newE2EPage();
    await page.setContent("<uwe-page></uwe-page>");

    const element = await page.find("uwe-page");
    expect(element).toHaveClass("hydrated");
  });
});
