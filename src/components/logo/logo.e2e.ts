import { newE2EPage } from "@stencil/core/testing";

describe("uwe-logo", () => {
  /**
   * Standard E2E Tests
   */
  it("renders", async () => {
    const page = await newE2EPage();
    await page.setContent("<uwe-logo></uwe-logo>");

    const element = await page.find("uwe-logo");
    expect(element).toHaveClass("hydrated");
  });

  it("renders alt text if no image given", async () => {
    const page = await newE2EPage();
    await page.setContent("<uwe-logo alt='Alternative Logo Text'></uwe-logo>");

    const element = await page.find("uwe-logo");
    expect(element).toHaveAttribute("alt");
    expect(element).toEqualAttribute("alt", "Alternative Logo Text");
  });

  /**
   * Screenshot Tests
   */
  it("renders default logo", async () => {
    const page = await newE2EPage();

    // Link to global CSS
    await page.setContent(
      '<link href="http://localhost:3333/build/uwe-ds.css" rel="stylesheet" /><uwe-logo image="default" alt="Alternative Logo Text" height="100"></uwe-logo>'
    );

    const element = await page.find("uwe-logo");
    expect(element).toHaveClass("hydrated");

    // Capture & compare screenshot
    const results = await page.compareScreenshot();
    expect(results).toMatchScreenshot();
  });

  it("renders secondary logo", async () => {
    const page = await newE2EPage();

    // Link to global CSS
    await page.setContent(
      '<link href="http://localhost:3333/build/uwe-ds.css" rel="stylesheet" /><uwe-logo image="secondary" alt="Alternative Logo Text" height="100"></uwe-logo>'
    );

    const element = await page.find("uwe-logo");
    expect(element).toHaveClass("hydrated");

    // Capture & compare screenshot
    const results = await page.compareScreenshot();
    expect(results).toMatchScreenshot();
  });

  it("renders alternative logo", async () => {
    const page = await newE2EPage();

    // Link to global CSS
    await page.setContent(
      '<link href="http://localhost:3333/build/uwe-ds.css" rel="stylesheet" /><uwe-logo image="alternative" alt="Alternative Logo Text" height="100"></uwe-logo>'
    );

    const element = await page.find("uwe-logo");
    expect(element).toHaveClass("hydrated");

    // Capture & compare screenshot
    const results = await page.compareScreenshot();
    expect(results).toMatchScreenshot();
  });

  it("renders su logo", async () => {
    const page = await newE2EPage();

    // Link to global CSS
    await page.setContent(
      '<link href="http://localhost:3333/build/uwe-ds.css" rel="stylesheet" /><uwe-logo image="su" alt="Alternative Logo Text" height="100"></uwe-logo>'
    );

    const element = await page.find("uwe-logo");
    expect(element).toHaveClass("hydrated");

    // Capture & compare screenshot
    const results = await page.compareScreenshot();
    expect(results).toMatchScreenshot();
  });

  it("renders su secondary logo", async () => {
    const page = await newE2EPage();

    // Link to global CSS
    await page.setContent(
      '<link href="http://localhost:3333/build/uwe-ds.css" rel="stylesheet" /><uwe-logo image="su-secondary" alt="Alternative Logo Text" height="100"></uwe-logo>'
    );

    const element = await page.find("uwe-logo");
    expect(element).toHaveClass("hydrated");

    // Capture & compare screenshot
    const results = await page.compareScreenshot();
    expect(results).toMatchScreenshot();
  });

  it("renders su inverted logo", async () => {
    const page = await newE2EPage();

    // Link to global CSS
    await page.setContent(
      '<link href="http://localhost:3333/build/uwe-ds.css" rel="stylesheet" /><uwe-logo image="su-inverted" alt="Alternative Logo Text" height="100"></uwe-logo>'
    );

    const element = await page.find("uwe-logo");
    expect(element).toHaveClass("hydrated");

    // Capture & compare screenshot
    const results = await page.compareScreenshot();
    expect(results).toMatchScreenshot();
  });
});
