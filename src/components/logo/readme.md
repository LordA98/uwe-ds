# uwe-logo

<!-- Auto Generated Below -->


## Properties

| Property | Attribute | Description                                                                                         | Type     | Default     |
| -------- | --------- | --------------------------------------------------------------------------------------------------- | -------- | ----------- |
| `alt`    | `alt`     | Alternative text for image                                                                          | `string` | `undefined` |
| `height` | `height`  | Logo Height                                                                                         | `string` | `undefined` |
| `image`  | `image`   | Chosen Logo Image Options: default (default), secondary, alternative, su, su-secondary, su-inverted | `string` | `"default"` |
| `width`  | `width`   | Logo Width                                                                                          | `string` | `undefined` |


## Dependencies

### Used by

 - [uwe-page](../page)

### Depends on

- [uwe-image](../image)

### Graph
```mermaid
graph TD;
  uwe-logo --> uwe-image
  uwe-page --> uwe-logo
  style uwe-logo fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
