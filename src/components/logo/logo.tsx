import { Component, h, Prop } from "@stencil/core";

/**
 * Logo Component.  Example Usage:
 * ```html
 * <uwe-logo
 *    image="default"
 *    alt="Alternative Text"
 *    height="100"
 * ></uwe-logo>
 * ```
 */
@Component({
  tag: "uwe-logo",
  styleUrl: "logo.css",
  shadow: true,
  assetsDirs: ["../../assets"]
})
export class Logo {
  /**
   * Alternative text for image
   * @category Optional
   */
  @Prop() alt?: string;
  /**
   * Logo Height
   * @category Optional
   */
  @Prop() height?: string;
  /**
   * Chosen Logo Image
   * Options: default (default), secondary, alternative, su, su-secondary, su-inverted
   * @category Optional
   */
  @Prop() image?: string = "default";
  /**
   * Logo Width
   * @category Optional
   */
  @Prop() width?: string;

  /**
   * @ignore
   * Render logo depending on image prop value.
   * The UNPKG CDN has to be used to store and render images as local assets
   * do not seem to work when integrated into other frameworks.
   */
  render() {
    return (
      <uwe-image
        src={`https://unpkg.com/uwe-ds@latest/dist/collection/assets/${this.image}.png`}
        alt={this.alt}
        height={this.height}
        width={this.width}
      />
    );
  }
}
