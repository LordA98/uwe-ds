import { Component, h, Prop } from "@stencil/core";

/**
 * Link Component.  Example Usage:
 * ```html
 * <uwe-link href="https://www.google.com">Self Link</uwe-link>
 * <uwe-link href="https://www.google.com" target="_blank"
 * >Blank Link</uwe-link
 * >
 * <uwe-link href="https://www.google.com" tooltip="This is a tooltip.">Tooltip</uwe-link>
 * ```
 */
@Component({
  tag: "uwe-link",
  styleUrl: "link.css",
  shadow: true
})
export class Link {
  /**
   * URL for anchor tag / link.
   * @category Required
   */
  @Prop() href!: string;

  /**
   * Target
   * Options: _blank, _self, _parent, _top, framename
   * framename - Opens the linked document in a named frame
   * Default: _self
   * @category Optional
   */
  @Prop() target?: string = "_self";

  /**
   * Title / Tooltip for link
   * @category Optional
   */
  @Prop() tooltip?: string;

  /** @ignore */
  render() {
    return (
      <a href={this.href} target={this.target} title={this.tooltip}>
        <slot />
      </a>
    );
  }
}
