import { newE2EPage } from "@stencil/core/testing";

describe("uwe-link", () => {
  /**
   * Standard E2E Tests
   */
  it("renders", async () => {
    const page = await newE2EPage();
    await page.setContent("<uwe-link></uwe-link>");

    const element = await page.find("uwe-link");
    expect(element).toHaveClass("hydrated");
  });

  it("displays correct text content", async () => {
    const page = await newE2EPage();
    await page.setContent("<uwe-link>Click me.</uwe-link>");

    const element = await page.find("uwe-link");
    expect(element).toEqualText("Click me.");
  });

  /**
   * Screenshot Tests
   */
  it("renders the link", async () => {
    const page = await newE2EPage();

    // Link to global CSS
    await page.setContent(
      '<link href="http://localhost:3333/build/uwe-ds.css" rel="stylesheet" /><uwe-link>Click me.</uwe-link>'
    );

    const element = await page.find("uwe-link");
    expect(element).toHaveClass("hydrated");

    // Capture & compare screenshot
    const results = await page.compareScreenshot();
    expect(results).toMatchScreenshot();
  });

  it("renders and responds to the tooltip property", async () => {
    const page = await newE2EPage();

    // Link to global CSS
    await page.setContent(
      '<link href="http://localhost:3333/build/uwe-ds.css" rel="stylesheet" /><uwe-link tooltip="I am a tooltip">Click me.</uwe-link>'
    );

    const element = await page.find("uwe-link");
    expect(element).toHaveClass("hydrated");
    expect(element).toHaveAttribute("tooltip");

    // Hover link to show tooltip
    await page.hover("uwe-link");

    // Capture & compare screenshot
    const results = await page.compareScreenshot();
    expect(results).toMatchScreenshot();
  });
});
