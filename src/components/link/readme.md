# uwe-link

<!-- Auto Generated Below -->


## Properties

| Property            | Attribute | Description                                                                                                                   | Type     | Default     |
| ------------------- | --------- | ----------------------------------------------------------------------------------------------------------------------------- | -------- | ----------- |
| `href` _(required)_ | `href`    | URL for anchor tag / link.                                                                                                    | `string` | `undefined` |
| `target`            | `target`  | Target Options: _blank, _self, _parent, _top, framename framename - Opens the linked document in a named frame Default: _self | `string` | `"_self"`   |
| `tooltip`           | `tooltip` | Title / Tooltip for link                                                                                                      | `string` | `undefined` |


## Dependencies

### Used by

 - [uwe-page](../page)

### Graph
```mermaid
graph TD;
  uwe-page --> uwe-link
  style uwe-link fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
