import { newE2EPage } from "@stencil/core/testing";

describe("uwe-paragraph", () => {
  /**
   * Standard E2E Tests
   */
  it("renders", async () => {
    const page = await newE2EPage();
    await page.setContent("<uwe-paragraph></uwe-paragraph>");

    const element = await page.find("uwe-paragraph");
    expect(element).toHaveClass("hydrated");
  });

  it("displays correct text content", async () => {
    const page = await newE2EPage();
    await page.setContent(
      "<uwe-paragraph>Paragraph about open days at UWE.</uwe-paragraph>"
    );

    const element = await page.find("uwe-paragraph");
    expect(element).toEqualText("Paragraph about open days at UWE.");
  });

  /**
   * Screenshot Tests
   */
  it("renders the paragraph", async () => {
    const page = await newE2EPage();

    // Link to global CSS
    await page.setContent(
      '<link href="http://localhost:3333/build/uwe-ds.css" rel="stylesheet" /><uwe-paragraph>Paragraph about open days at UWE.</uwe-paragraph>'
    );

    const element = await page.find("uwe-paragraph");
    expect(element).toHaveClass("hydrated");

    // Capture & compare screenshot
    const results = await page.compareScreenshot();
    expect(results).toMatchScreenshot();
  });
});
