# uwe-paragraph

<!-- Auto Generated Below -->


## Dependencies

### Used by

 - [uwe-page](../page)

### Graph
```mermaid
graph TD;
  uwe-page --> uwe-paragraph
  style uwe-paragraph fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
