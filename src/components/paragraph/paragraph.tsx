import { Component, h } from "@stencil/core";

/**
 * Paragraph Component.  Example Usage:
 * ```html
 * <uwe-paragraph>
 *  This is a paragraph with the UWE styles.
 * </uwe-paragraph>
 * ```
 */
@Component({
  tag: "uwe-paragraph",
  styleUrl: "paragraph.css",
  shadow: true
})
export class Paragraph {
  /** @ignore */
  render() {
    return (
      <p>
        <slot />
      </p>
    );
  }
}
