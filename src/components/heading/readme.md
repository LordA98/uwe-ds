# uwe-heading

<!-- Auto Generated Below -->


## Properties

| Property | Attribute | Description                                    | Type     | Default |
| -------- | --------- | ---------------------------------------------- | -------- | ------- |
| `size`   | `size`    | Heading Size Options: 1-6 (h1 - h6) Default: 1 | `number` | `1`     |


## Dependencies

### Used by

 - [uwe-page](../page)

### Graph
```mermaid
graph TD;
  uwe-page --> uwe-heading
  style uwe-heading fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
