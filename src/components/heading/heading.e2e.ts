import { newE2EPage } from "@stencil/core/testing";

describe("uwe-heading", () => {
  /**
   * Standard E2E Tests
   */
  it("renders", async () => {
    const page = await newE2EPage();
    await page.setContent("<uwe-heading>Open Days Heading</uwe-heading>");

    const element = await page.find("uwe-heading");
    expect(element).not.toBeNull();
    expect(element).toHaveClass("hydrated");
  });

  it("displays correct text content", async () => {
    const page = await newE2EPage();
    await page.setContent("<uwe-heading>Open Days Heading</uwe-heading>");

    const element = await page.find("uwe-heading");
    expect(element).toEqualText("Open Days Heading");
  });

  it("renders h1", async () => {
    const page = await newE2EPage();
    await page.setContent(
      "<uwe-heading size='1'>Open Days Heading</uwe-heading>"
    );

    const element = await page.find("uwe-heading >>> h1");
    expect(element).toEqualHtml(
      `
        <h1>
          <slot />
        </h1>
      `
    );
  });

  it("renders h2", async () => {
    const page = await newE2EPage();
    await page.setContent(
      "<uwe-heading size='2'>Open Days Heading</uwe-heading>"
    );

    const element = await page.find("uwe-heading >>> h2");
    expect(element).toEqualHtml(
      `
        <h2>
          <slot />
        </h2>
      `
    );
  });

  it("renders h3", async () => {
    const page = await newE2EPage();
    await page.setContent(
      "<uwe-heading size='3'>Open Days Heading</uwe-heading>"
    );

    const element = await page.find("uwe-heading >>> h3");
    expect(element).toEqualHtml(
      `
        <h3>
          <slot />
        </h3>
      `
    );
  });

  it("renders h4", async () => {
    const page = await newE2EPage();
    await page.setContent(
      "<uwe-heading size='4'>Open Days Heading</uwe-heading>"
    );

    const element = await page.find("uwe-heading >>> h4");
    expect(element).toEqualHtml(
      `
        <h4>
          <slot />
        </h4>
      `
    );
  });

  it("renders h5", async () => {
    const page = await newE2EPage();
    await page.setContent(
      "<uwe-heading size='5'>Open Days Heading</uwe-heading>"
    );

    const element = await page.find("uwe-heading >>> h5");
    expect(element).toEqualHtml(
      `
        <h5>
          <slot />
        </h5>
      `
    );
  });

  it("renders h6", async () => {
    const page = await newE2EPage();
    await page.setContent(
      "<uwe-heading size='6'>Open Days Heading</uwe-heading>"
    );

    const element = await page.find("uwe-heading >>> h6");
    expect(element).toEqualHtml(
      `
        <h6>
          <slot />
        </h6>
      `
    );
  });

  it("renders h1 if no size property given (default)", async () => {
    const page = await newE2EPage();
    await page.setContent("<uwe-heading>Open Days Heading</uwe-heading>");

    const element = await page.find("uwe-heading >>> h1");
    expect(element).toEqualHtml(
      `
        <h1>
          <slot />
        </h1>
      `
    );
  });

  /**
   * Screenshot Tests
   */
  it("renders and responds to size property (1)", async () => {
    const page = await newE2EPage();

    // Link to global CSS
    await page.setContent(
      '<link href="http://localhost:3333/build/uwe-ds.css" rel="stylesheet" /><uwe-heading size="1">Open Days Heading</uwe-heading>'
    );

    const element = await page.find("uwe-heading");
    expect(element).toHaveClass("hydrated");

    // Capture & compare screenshot
    const results = await page.compareScreenshot();
    expect(results).toMatchScreenshot();
  });

  it("renders and responds to size property (2)", async () => {
    const page = await newE2EPage();

    // Link to global CSS
    await page.setContent(
      '<link href="http://localhost:3333/build/uwe-ds.css" rel="stylesheet" /><uwe-heading size="2">Open Days Heading</uwe-heading>'
    );

    const element = await page.find("uwe-heading");
    expect(element).toHaveClass("hydrated");

    // Capture & compare screenshot
    const results = await page.compareScreenshot();
    expect(results).toMatchScreenshot();
  });

  it("renders and responds to size property (3)", async () => {
    const page = await newE2EPage();

    // Link to global CSS
    await page.setContent(
      '<link href="http://localhost:3333/build/uwe-ds.css" rel="stylesheet" /><uwe-heading size="3">Open Days Heading</uwe-heading>'
    );

    const element = await page.find("uwe-heading");
    expect(element).toHaveClass("hydrated");

    // Capture & compare screenshot
    const results = await page.compareScreenshot();
    expect(results).toMatchScreenshot();
  });

  it("renders and responds to size property (4)", async () => {
    const page = await newE2EPage();

    // Link to global CSS
    await page.setContent(
      '<link href="http://localhost:3333/build/uwe-ds.css" rel="stylesheet" /><uwe-heading size="4">Open Days Heading</uwe-heading>'
    );

    const element = await page.find("uwe-heading");
    expect(element).toHaveClass("hydrated");

    // Capture & compare screenshot
    const results = await page.compareScreenshot();
    expect(results).toMatchScreenshot();
  });

  it("renders and responds to size property (5)", async () => {
    const page = await newE2EPage();

    // Link to global CSS
    await page.setContent(
      '<link href="http://localhost:3333/build/uwe-ds.css" rel="stylesheet" /><uwe-heading size="5">Open Days Heading</uwe-heading>'
    );

    const element = await page.find("uwe-heading");
    expect(element).toHaveClass("hydrated");

    // Capture & compare screenshot
    const results = await page.compareScreenshot();
    expect(results).toMatchScreenshot();
  });

  it("renders and responds to size property (6)", async () => {
    const page = await newE2EPage();

    // Link to global CSS
    await page.setContent(
      '<link href="http://localhost:3333/build/uwe-ds.css" rel="stylesheet" /><uwe-heading size="6">Open Days Heading</uwe-heading>'
    );

    const element = await page.find("uwe-heading");
    expect(element).toHaveClass("hydrated");

    // Capture & compare screenshot
    const results = await page.compareScreenshot();
    expect(results).toMatchScreenshot();
  });
});
