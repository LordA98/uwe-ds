import { Component, h, Prop } from "@stencil/core";

/**
 * Heading Component.  Example Usage:
 * ```html
 * <uwe-heading size="1">h1</uwe-heading>
 * <uwe-heading size="2">h2</uwe-heading>
 * <uwe-heading size="3">h3</uwe-heading>
 * <uwe-heading size="4">h4</uwe-heading>
 * <uwe-heading size="5">h5</uwe-heading>
 * <uwe-heading size="6">h6</uwe-heading>
 * ```
 */
@Component({
  tag: "uwe-heading",
  styleUrl: "heading.css",
  shadow: true
})
export class Heading {
  /**
   * Heading Size
   * Options: 1-6 (h1 - h6)
   * Default: 1
   * @category Optional
   */
  @Prop() size: number = 1;

  /** @ignore */
  render() {
    switch (this.size) {
      case 1: {
        return (
          <div>
            <h1>
              <slot />
            </h1>
          </div>
        );
      }
      case 2: {
        return (
          <h2>
            <slot />
          </h2>
        );
      }
      case 3: {
        return (
          <h3>
            <slot />
          </h3>
        );
      }
      case 4: {
        return (
          <h4>
            <slot />
          </h4>
        );
      }
      case 5: {
        return (
          <h5>
            <slot />
          </h5>
        );
      }
      case 6: {
        return (
          <h6>
            <slot />
          </h6>
        );
      }
      default: {
        return (
          <div>
            <h1>
              <slot />
            </h1>
          </div>
        );
      }
    }
  }
}
