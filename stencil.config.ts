import { Config } from "@stencil/core";
import { postcss } from "@stencil/postcss";
import autoprefixer from "autoprefixer";

const purge = require("@fullhuman/postcss-purgecss")({
  content: ["./src/**/*.tsx", "./src/**/*.css", "./src/index.html"],
  defaultExtractor: content => content.match(/[A-Za-z0-9-_:/]+/g) || []
});

export const config: Config = {
  namespace: "uwe-ds",
  globalStyle: "src/global/global.css",
  outputTargets: [
    {
      type: "dist",
      esmLoaderPath: "../loader"
    },
    {
      type: "docs-readme"
    },
    {
      type: "www",
      serviceWorker: null, // disable service workers
      baseUrl: "http://localhost:5000",
      copy: [{ src: "assets", dest: "build/assets" }]
    }
  ],
  plugins: [
    postcss({
      plugins: [
        require("tailwindcss")("./tailwind.config.js"),
        autoprefixer(),
        ...(process.env.NODE_ENV === "production"
          ? [purge, require("cssnano")]
          : [])
      ]
    })
  ],
  bundles: [
    { components: ["uwe-button", "uwe-link"] },
    { components: ["uwe-paragraph"] },
    { components: ["uwe-heading"] },
    { components: ["uwe-image", "uwe-logo"] },
    {
      components: [
        "uwe-form",
        "uwe-input",
        "uwe-input-group",
        "uwe-label",
        "uwe-select",
        "uwe-option",
        "uwe-textarea"
      ]
    }
  ]
};
