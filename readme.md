![Built With Stencil](https://img.shields.io/badge/-Built%20With%20Stencil-16161d.svg?logo=data%3Aimage%2Fsvg%2Bxml%3Bbase64%2CPD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0idXRmLTgiPz4KPCEtLSBHZW5lcmF0b3I6IEFkb2JlIElsbHVzdHJhdG9yIDE5LjIuMSwgU1ZHIEV4cG9ydCBQbHVnLUluIC4gU1ZHIFZlcnNpb246IDYuMDAgQnVpbGQgMCkgIC0tPgo8c3ZnIHZlcnNpb249IjEuMSIgaWQ9IkxheWVyXzEiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgeG1sbnM6eGxpbms9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsiIHg9IjBweCIgeT0iMHB4IgoJIHZpZXdCb3g9IjAgMCA1MTIgNTEyIiBzdHlsZT0iZW5hYmxlLWJhY2tncm91bmQ6bmV3IDAgMCA1MTIgNTEyOyIgeG1sOnNwYWNlPSJwcmVzZXJ2ZSI%2BCjxzdHlsZSB0eXBlPSJ0ZXh0L2NzcyI%2BCgkuc3Qwe2ZpbGw6I0ZGRkZGRjt9Cjwvc3R5bGU%2BCjxwYXRoIGNsYXNzPSJzdDAiIGQ9Ik00MjQuNywzNzMuOWMwLDM3LjYtNTUuMSw2OC42LTkyLjcsNjguNkgxODAuNGMtMzcuOSwwLTkyLjctMzAuNy05Mi43LTY4LjZ2LTMuNmgzMzYuOVYzNzMuOXoiLz4KPHBhdGggY2xhc3M9InN0MCIgZD0iTTQyNC43LDI5Mi4xSDE4MC40Yy0zNy42LDAtOTIuNy0zMS05Mi43LTY4LjZ2LTMuNkgzMzJjMzcuNiwwLDkyLjcsMzEsOTIuNyw2OC42VjI5Mi4xeiIvPgo8cGF0aCBjbGFzcz0ic3QwIiBkPSJNNDI0LjcsMTQxLjdIODcuN3YtMy42YzAtMzcuNiw1NC44LTY4LjYsOTIuNy02OC42SDMzMmMzNy45LDAsOTIuNywzMC43LDkyLjcsNjguNlYxNDEuN3oiLz4KPC9zdmc%2BCg%3D%3D&colorA=16161d&style=flat-square)

# UWE Design System

## Basic Installation & Usage

_Refer to 'Framework Integration' section for specific instructions for React, Vue & Angular integration._

### NPM Installation

`npm i uwe-ds`

### Use UNPKG CDN

Vanilla JavaScript projects can simply use the CDN. Place the following links are the top of your index.html page.

```
<link rel="stylesheet" type="text/css" href="https://unpkg.com/uwe-ds@latest/dist/uwe-ds/uwe-ds.css" />
<script type="module" src="https://unpkg.com/uwe-ds@latest/dist/uwe-ds/uwe-ds.esm.js"></script>
<script nomodule src="https://unpkg.com/uwe-ds@latest/dist/uwe-ds/uwe-ds.js"></script>
```

## Framework Integrations

### React

With a React app built with create-react-app, two lines need to be added to the index.js file (after installing the uwe-ds package):

```
// Other imports...

// Add this one
import { applyPolyfills, defineCustomElements } from 'uwe-ds/loader';

ReactDOM.render(<App />, document.getElementById('root'));

// Has not effect on design system - optional.
serviceWorker.unregister();

// and add this one
applyPolyfills().then(() => {
    defineCustomElements(window);
});
```

UWE components should be usable inside other React components now.

Sometimes the CSS styling doesn't get applied to components. In order to get this to work, add a link to the CSS in public/index.html:

```
<!-- UWE Design System CSS CDN -->
<link rel="stylesheet" type="text/css" href="https://unpkg.com/uwe-ds@latest/dist/uwe-ds/uwe-ds.css"/>
```

**Helpful Resources**: [Stencil Documentation for React](https://stenciljs.com/docs/react)

### Vue

Assuming that the package has been installed with `npm i uwe-ds` beforehand, your main.js file should look like this:

```
// Other imports

// Add this line
import { applyPolyfills, defineCustomElements } from 'uwe-ds/loader';

Vue.config.productionTip = false;

// Add this line (Tells Vue to ignore all UWE components)
Vue.config.ignoredElements = [/uwe-\w*/];

// Add this line (Bind the custom elements to the window object)
applyPolyfills().then(() => {
  defineCustomElements(window);
});

new Vue({
  render: h => h(App)
}).$mount('#app');
```

UWE components should be usable inside other Vue components now.

Sometimes the CSS styling doesn't get applied to components. In order to get this to work, add a link to the CSS in public/index.html:

```
<!-- UWE Design System CSS CDN -->
<link rel="stylesheet" type="text/css" href="https://unpkg.com/uwe-ds@latest/dist/uwe-ds/uwe-ds.css"/>
```

**Helpful Resources**: [Stencil Documentation for Vue](https://stenciljs.com/docs/vue)

### Angular

Assuming that the package has been installed with `npm i uwe-ds` beforehand, your main.ts file should look like this:

```
// Other imports

// Add this
import { applyPolyfills, defineCustomElements } from 'uwe-ds/loader';

if (environment.production) {
  enableProdMode();
}

platformBrowserDynamic().bootstrapModule(AppModule)
  .catch(err => console.error(err));

// Add this
applyPolyfills().then(() => {
  defineCustomElements(window)
})
```

Add this into the App Module (app.module.ts) and every other module that will use the components:

```
import { BrowserModule } from '@angular/platform-browser';
// Add CUSTOM_ELEMENTS_SCHEMA - prevents errors when using UWE components
import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';

import { AppComponent } from './app.component';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent],
  // Add this
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AppModule { }
```

Sometimes the CSS styling doesn't get applied to components. In order to get this to work, add a link to the CSS in src/index.html:

```
<!-- UWE Design System CSS CDN -->
<link rel="stylesheet" type="text/css" href="https://unpkg.com/uwe-ds@latest/dist/uwe-ds/uwe-ds.css"/>
```

**Helpful Resources**: [Stencil Documentation for Angular](https://stenciljs.com/docs/angular)
