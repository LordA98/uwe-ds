var replace = require("replace");
const args = require("minimist")(process.argv.slice(2));

const capitalize = s => {
  if (typeof s !== "string") return "";
  return s.charAt(0).toUpperCase() + s.slice(1);
};

// console.log(args);

replace({
  regex: "uwe-",
  replacement: `${args["abbv"]}-`,
  paths: ["."],
  recursive: true,
  exclude:
    "reconfigure.js, node_modules, .stencil, assets, dist, docs, loader, www, yarn.lock, package-lock.json, .gitignore, .editorconfig, .pa11yci, .prettierrc.json, commitlint.config.js, pa11y-sitemap.xml, tailwind.config.js, tsconfig.json, tslint.json"
});

replace({
  regex: "Uwe",
  replacement: capitalize(args["abbv"]),
  paths: ["."],
  recursive: true,
  exclude:
    "reconfigure.js, node_modules, .stencil, assets, dist, docs, loader, www, yarn.lock, package-lock.json, .gitignore, .editorconfig, .pa11yci, .prettierrc.json, commitlint.config.js, pa11y-sitemap.xml, tailwind.config.js, tsconfig.json, tslint.json"
});

replace({
  regex: "UWE",
  replacement: args["name"],
  paths: ["."],
  recursive: true,
  exclude:
    "reconfigure.js, node_modules, .stencil, assets, dist, docs, loader, www, yarn.lock, package-lock.json, .gitignore, .editorconfig, .pa11yci, .prettierrc.json, commitlint.config.js, pa11y-sitemap.xml, tailwind.config.js, tsconfig.json, tslint.json"
});
